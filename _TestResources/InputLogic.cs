﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputLogic : MonoBehaviour
{
    private static InputLogic instance;
    public GameObject[] positionInputFields;
    public InputField[] orderedInputFields;
    private int index;
    private InputField.ContentType contentType;

    [HideInInspector]
    public int indexInputField = 0;
    [HideInInspector]
    public InputField inputFieldSelected = null;
    [HideInInspector]
    public string userAnswer="";
    private InputLogic()
    {

    }
    void Awake()
    {
        
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        inputFieldSelected = orderedInputFields[0];
        for (int i = 0; i < orderedInputFields.Length; i++)
        {
            orderedInputFields[i].shouldHideMobileInput = true;
        }

    }
    public static InputLogic GetInstance()
    {
        return instance;
    }
    void OnEnable()
    {
        //Activate how input fields user choosed
        for (int i = 0; i < positionInputFields.Length; i++)
        {
            positionInputFields[i].SetActive(false);
        }
        for (int i = 0; i < GlobalParameters.lengthList; i++)
        {
            positionInputFields[i].SetActive(true);
        }
        for (int i = 0; i < orderedInputFields.Length; i++)
        {
            orderedInputFields[i].text = "";
        } 
        userAnswer = "";

        //Set the input type choosed by user
        if (GlobalParameters.gameType == GameType.AlphaNumeric)
        {
            contentType = InputField.ContentType.Alphanumeric;
        }
        else if (GlobalParameters.gameType == GameType.Numeric)
        {
            contentType = InputField.ContentType.IntegerNumber;
        }
        else if (GlobalParameters.gameType == GameType.Letters)
        {
            contentType = InputField.ContentType.Name;
        }

        for (int i = 0; i < orderedInputFields.Length; i++)
        {
            orderedInputFields[i].contentType = contentType;
            orderedInputFields[i].shouldHideMobileInput = true;
        }

        EventSystem.current.SetSelectedGameObject(orderedInputFields[0].gameObject);
        orderedInputFields[0].OnPointerClick(new PointerEventData(EventSystem.current));
        //inputFieldSelected = orderedInputFields[0];
    }

    [ContextMenu("ShowDebugInfo")]
    public void DebugInfo()
    {
        Debug.Log("Show:" + indexInputField);
        Debug.Log("NumeShow:"+EventSystem.current.currentSelectedGameObject.gameObject.name);
    }
    public void OnCheckInput()
    {

        if (orderedInputFields[indexInputField].isFocused && orderedInputFields[indexInputField].text.Length > 0)
        {
            //inputFieldSelected = orderedInputFields[indexInputField];
            CheckInput();
           
        }


    }
    public void CheckInput()
    {
        if (indexInputField < orderedInputFields.Length - 1)
        {
            TestInputSelect input = orderedInputFields[++indexInputField].gameObject.GetComponent<TestInputSelect>();
            if (input.block)
                EventSystem.current.SetSelectedGameObject(orderedInputFields[indexInputField].gameObject);
            orderedInputFields[indexInputField].OnPointerClick(new PointerEventData(EventSystem.current));
        }
        else
        {

            TestInputSelect.canGoForward = true;
        }
    }

    public void ChecksSelectedPosition(int _position)
    {
        indexInputField = _position;
    }
    public void OnCheckAnswer()
    {
        string tmpAnswer = "";
        for (int i = 0; i < GlobalParameters.lengthList; i++)
        {
            tmpAnswer+=orderedInputFields[i].text;
        }
        userAnswer = tmpAnswer;
        Debug.Log("Answer:" + tmpAnswer);
    }
}
