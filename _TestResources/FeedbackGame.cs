﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedbackGame : MonoBehaviour {

    public StarAnimation[] stars;

    void OnEnable()
    {
        foreach(StarAnimation _star in stars)
        {
            _star.startAnimation = true;
        }
    }

    void OnDisable()
    {
        foreach (StarAnimation _star in stars)
        {
            _star.startAnimation = false;
            _star.ResetAnimation();
        }
    }
}
