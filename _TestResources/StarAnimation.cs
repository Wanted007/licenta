﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarAnimation : MonoBehaviour {

    public GameObject target;
    public float speed = 5.0f;
    public float alphaSpeed = 10f;

    public bool startAnimation = false;

    private Vector2 startPosition;
    private CanvasGroup canvasGroup;
    void Start()
    {
        startPosition = (Vector2)transform.position;
        canvasGroup = GetComponent<CanvasGroup>();

    }
    void FixedUpdate()
    {
        if (startAnimation)
        {
            Vector2 pos = Vector2.Lerp((Vector2)transform.position, (Vector2)target.transform.position, Time.fixedDeltaTime * speed);
            transform.position = new Vector3(pos.x, pos.y, 0);
            
        }
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }
    void Update()
    {
        if (startAnimation)
        {
            canvasGroup.alpha = Mathf.Lerp(0, 1, Time.fixedTime * alphaSpeed);
        }
    }
    [ContextMenu("ResetAnimation")]
    public void ResetAnimation()
    {
        startAnimation = false;
        transform.position = new Vector3(startPosition.x, startPosition.y, transform.position.y);
        canvasGroup.alpha = 0;
    }
}
