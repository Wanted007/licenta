﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;

public class Statistics : MonoBehaviour
{

    //Actual time
    int mCurrentDay;
    int mCurrentWeek;
    int mCurrentMonth;
    int mCurrentYear;

    //Player prefs time
    int mSavedDay;
    int mSavedWeek;
    int mSavedMonth;
    int mSavedYear;

    //Last time-> for comparing(performance)
    int mLastDay;
    int mLastWeek;
    int mLastMonth;

    float CalculateBonus()
    {
        int tmpTypeGame = 0;
        switch (GlobalParameters.gameType)
        {
            case GameType.Numeric:
                tmpTypeGame = 2;
                break;
            case GameType.Letters:
                tmpTypeGame = 4;
                break;
            case GameType.AlphaNumeric:
                tmpTypeGame = 6;
                break;
            default:
                tmpTypeGame = 0;
                break;
        }

        return 0.1f * GlobalParameters.lengthList * tmpTypeGame;
    }
    public void CalculateTime()
    {
        mCurrentDay = DateTime.Today.Day;
        mCurrentWeek = DateTime.Today.GetWeekOfMonth();
        mCurrentMonth = DateTime.Today.Month;
        mCurrentYear = DateTime.Today.Year;

        mSavedDay = PlayerPrefs.GetInt(Performance.DayKey);
        mSavedWeek = PlayerPrefs.GetInt(Performance.WeekKey);
        mSavedMonth = PlayerPrefs.GetInt(Performance.MonthKey);
        mSavedYear = PlayerPrefs.GetInt(Performance.YearKey);
    }

    public void OnCalculateBonus()
    {
        CalculateTime();
        UpdateDay(CalculateBonus());
        UpdateMonth(CalculateBonus());
        UpdateWeek(CalculateBonus());
    }


    public void UpdateDay(float _bonusGame = 0)
    {
        if (mCurrentDay != mSavedDay)
        {
            UpdateLastTime("day");
            PlayerPrefs.SetFloat(Performance.BonusDayKey, 0);
            PlayerPrefs.SetInt(Performance.DayKey, mCurrentDay);
        }

        float tmpBonus = PlayerPrefs.GetFloat(Performance.BonusDayKey);
        tmpBonus += _bonusGame;
        PlayerPrefs.SetFloat(Performance.BonusDayKey, tmpBonus);
    }

    public void UpdateMonth(float _bonusGame = 0)
    {
        if (mCurrentMonth != mSavedMonth)
        {
            UpdateLastTime("month");
            PlayerPrefs.SetFloat(Performance.BonusMonthKey, 0);
            PlayerPrefs.SetInt(Performance.MonthKey, mCurrentMonth);
        }

        float tmpBonus = PlayerPrefs.GetFloat(Performance.BonusMonthKey);
        tmpBonus += _bonusGame;
        PlayerPrefs.SetFloat(Performance.BonusMonthKey, tmpBonus);

        float tmpTotalBonus = PlayerPrefs.GetFloat(Performance.TotalBonus);
        tmpTotalBonus += _bonusGame;
        PlayerPrefs.SetFloat(Performance.TotalBonus, tmpTotalBonus);
    }

    public void UpdateWeek(float _bonusGame = 0)
    {
        if (mCurrentWeek != mSavedWeek)
        {
            UpdateLastTime("week");
            PlayerPrefs.SetFloat(Performance.BonusWeekKey, 0);
            PlayerPrefs.SetInt(Performance.WeekKey, mCurrentWeek);
        }

        float tmpBonus = PlayerPrefs.GetFloat(Performance.BonusWeekKey);
        tmpBonus += _bonusGame;
        PlayerPrefs.SetFloat(Performance.BonusWeekKey, tmpBonus);
    }

    public void UpdateLastTime(string _value)
    {
        switch (_value)
        {
            case "day":
                PlayerPrefs.SetInt(Performance.LastDayPlayedKey, mSavedDay);
                PlayerPrefs.SetFloat(Performance.BonusLastDayPlayedKey, PlayerPrefs.GetFloat(Performance.BonusDayKey));
                break;
            case "week":
                PlayerPrefs.SetInt(Performance.LastWeekPlayedKey, mSavedWeek);
                PlayerPrefs.SetFloat(Performance.BonusLastWeekPlayedKey, PlayerPrefs.GetFloat(Performance.BonusWeekKey));
                break;
            case "month":
                PlayerPrefs.SetInt(Performance.LastMonthPlayedKey, mSavedMonth);
                PlayerPrefs.SetFloat(Performance.BonusLastMonthPlayedKey, PlayerPrefs.GetFloat(Performance.BonusMonthKey));
                break;

            default:
                break;
        }
    }

    [ContextMenu("TestNow")]
    public void Test()
    {

        //DateTime date2 = DateTime.Today;

        //Debug.Log(DateTime.Today.Day);
        //Debug.Log(DateTime.Today.Month);
        //Debug.Log(DateTime.Today.DayOfWeek);
        //Debug.Log(DateTime.Today.Year);

        //Debug.Log(date2.GetWeekOfMonth());

        Debug.Log("Ziua:"+PlayerPrefs.GetInt(Performance.DayKey));
        Debug.Log(PlayerPrefs.GetFloat(Performance.BonusDayKey));

        Debug.Log("ZiuaAnterioară:" + PlayerPrefs.GetInt(Performance.LastDayPlayedKey));
        Debug.Log(PlayerPrefs.GetFloat(Performance.BonusLastDayPlayedKey));

        Debug.Log("Saptamana:"+PlayerPrefs.GetInt(Performance.WeekKey));
        Debug.Log(PlayerPrefs.GetFloat(Performance.BonusWeekKey));
        Debug.Log("Luna:"+PlayerPrefs.GetInt(Performance.MonthKey));
        Debug.Log(PlayerPrefs.GetFloat(Performance.BonusMonthKey));
    }

    [ContextMenu("DeleteSavedData")]
    public void DeleteSavedData()
    {
        PlayerPrefs.DeleteAll();
    }



}

//Class with extension methods for calculate the week of the month :)
public static class CalculateTime
{
    static GregorianCalendar _gc = new GregorianCalendar();
    public static int GetWeekOfMonth(this DateTime time)
    {
        DateTime first = new DateTime(time.Year, time.Month, 1);
        return time.GetWeekOfYear() - first.GetWeekOfYear() + 1;
    }
    static int GetWeekOfYear(this DateTime time)
    {
        return _gc.GetWeekOfYear(time, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
    }
}
