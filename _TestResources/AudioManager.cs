﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    private static AudioManager instance = null;

    public AudioSource clickSoundSource;
    public AudioSource backgroundSource;
    public AudioSource winloseSource;

    public AudioClip clickSound;
    public AudioClip winSound;
    public AudioClip loseSound;

    public Sprite volumeOn;
    public Sprite volumeOff;

    public Image settingsImage;

    private AudioManager()
    {
    }
    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);


        DontDestroyOnLoad(gameObject); 
    }
    public static AudioManager GetInstance()
    {
        return instance;
    }
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (EventSystem.current.currentSelectedGameObject != null && EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
            {
                clickSoundSource.Play();
            }
           
        }
    }
    public void OnMute()
    {
        clickSoundSource.enabled = !clickSoundSource.enabled;
        backgroundSource.enabled= !backgroundSource.enabled;
        winloseSource.enabled = !winloseSource.enabled;

        settingsImage.sprite = (clickSoundSource.enabled == true) ? volumeOn : volumeOff;

    }
    public void OnWin()
    {
        backgroundSource.volume = 0.0f;
        winloseSource.clip = winSound;
        float soundLength = winSound.length;
        winloseSource.Play();
        Invoke("OnResetBackgroundVolume", soundLength+0.5f);

    }
    public void OnResetBackgroundVolume()
    {
        backgroundSource.volume = 0.5f;
    }
    public  void OnLose()
    {
        backgroundSource.volume = 0.0f;
        winloseSource.clip = loseSound;
        float soundLength = loseSound.length;
        winloseSource.Play();
        Invoke("OnResetBackgroundVolume", soundLength+0.5f);

    }
}
