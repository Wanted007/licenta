﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackButtonFunctionality : MonoBehaviour
{
    //#if UNITY_ANDROID
    public GameObject[] mPages;
    public static int mContorPages = 0;

    public void OnNextPage()
    {
        if (mContorPages < mPages.Length)
            BackButtonFunctionality.mContorPages++;
    }
    public void OnPrevPage()
    {
        if (mContorPages > 0)
            BackButtonFunctionality.mContorPages--;
    }
    public void OnSetIndexPage(int _indexPage)
    {
        if (_indexPage >= 0 && _indexPage< mPages.Length)
        {
            BackButtonFunctionality.mContorPages = _indexPage;
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Pas:" + BackButtonFunctionality.mContorPages);
            if(mContorPages==5)
            {
                mContorPages = 1;
                mPages[BackButtonFunctionality.mContorPages].SetActive(true);
                mPages[BackButtonFunctionality.mContorPages].transform.SetAsLastSibling();
                mPages[3].SetActive(false);
            }
            else if (mContorPages - 1 >= 0)
            {
                Debug.Log("Intrat");

                mPages[BackButtonFunctionality.mContorPages - 1].SetActive(true);
                mPages[BackButtonFunctionality.mContorPages - 1].transform.SetAsLastSibling();
                mPages[BackButtonFunctionality.mContorPages].SetActive(false);
                BackButtonFunctionality.mContorPages--;
            }
        }
    }
    //#endif

}
