﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class KeyboardFunctionality : MonoBehaviour
{

    public GameObject[] mTextsInput;
    public GameObject[] mKeyboardLetters;
    public static int mContor = 0;
    public Color mHighlightColor;
    public Color mBlockColor;
    public Image lockImage;
    public float lockSpeed = 0.1f;
    public Text mInfoText;
    public bool mIsLock = false;
    float time = 0;
    private static KeyboardFunctionality instance;
    void Awake()
    {

        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }

        else
        {
            //Destroy(gameObject);
        }
    }
    public static KeyboardFunctionality GetInstance()
    {
        return instance;
    }
    void OnEnable()
    {
        OnResetGame();

    }
    public void OnResetGame()
    {
        for (int i = 0; i < mTextsInput.Length; i++)
        {
            mTextsInput[i].SetActive(false);
        }
        for (int i = 0; i < GlobalParameters.lengthList; i++)
        {
            mTextsInput[i].SetActive(true);
            mTextsInput[i].transform.GetChild(1).GetComponent<Text>().text = "";
        }
    }
    public void OnAnswer(Text _text)
    {
        if (mContor < mTextsInput.Length)
        {
            if (!mTextsInput[mContor].GetComponent<Image>().enabled)
            {
                mContor++;
            }
            else
            {
                mTextsInput[mContor++].transform.GetChild(1).GetComponent<Text>().text = _text.text;
                //_text.gameObject.GetComponentInParent<Button>().interactable = false;
                CheckLetters();
                ResetColor();
                mTextsInput[mContor].transform.GetChild(0).GetComponent<Image>().color = mHighlightColor;
            }
        }

    }

    void FixedUpdate()
    {

        if (mTextsInput[mContor].GetComponent<Image>().enabled)
        {
            mInfoText.text = "Lock";
            mIsLock = true;

        }
        else
        {
            mIsLock = false;
            mInfoText.text = "Unlock";
        }
    }
    void ResetColor()
    {
        for (int i = 0; i < mTextsInput.Length; i++)
        {
            if (mTextsInput[i].GetComponent<Image>().enabled)
            {
                mTextsInput[i].transform.GetChild(0).GetComponent<Image>().color = Color.white;
            }
        }
    }
    public void OnSetCustomContor(int _contor)
    {
        mContor = _contor;
        ResetColor();
        if (mTextsInput[mContor].GetComponent<Image>().enabled)
        {
            mTextsInput[mContor].transform.GetChild(0).GetComponent<Image>().color = mHighlightColor;
            lockImage.fillAmount = 0.5f;
        }
        else
        {
            lockImage.fillAmount = 1.0f;
        }
        CheckLetters();
    }
    public void OnBlockButton()
    {
        ResetColor();

        if (mTextsInput[mContor].GetComponent<Image>().enabled)
        {

            mTextsInput[mContor].GetComponent<Image>().enabled = false;
            mTextsInput[mContor].transform.GetChild(0).GetComponent<Image>().color = mBlockColor;
            mTextsInput[mContor].transform.GetChild(1).GetComponent<Text>().color = mBlockColor;
            mContor += 1;
            //mIsLock = true;
            mTextsInput[mContor].transform.GetChild(0).GetComponent<Image>().color = mHighlightColor;
            //lockImage.fillAmount = 0.5f;
            StartCoroutine(LockImage());
        }
        else
        {
            //mIsLock = false;
            StartCoroutine(LockImage());
            mTextsInput[mContor].GetComponent<Image>().enabled = true;
            mTextsInput[mContor].transform.GetChild(0).GetComponent<Image>().color = mHighlightColor;
            mTextsInput[mContor].transform.GetChild(1).GetComponent<Text>().color = Color.white;
        }


    }

    public void ResetText()
    {
        for (int i = 0; i < mTextsInput.Length; i++)
        {
            mTextsInput[i].transform.GetChild(1).GetComponent<Text>().text = "";
        }
        for (int i = 0; i < mKeyboardLetters.Length; i++)
        {
            mKeyboardLetters[i].GetComponent<Button>().interactable = true;
        }
        OnSetCustomContor(0);
    }

    public void CheckLetters()
    {
        for (int i = 0; i < mKeyboardLetters.Length; i++)
        {
            for (int j = 0; j < mTextsInput.Length; j++)
            {
                if (mKeyboardLetters[i].transform.GetChild(0).GetComponent<Text>().text == mTextsInput[j].transform.GetChild(1).GetComponent<Text>().text)
                {
                    mKeyboardLetters[i].GetComponent<Button>().interactable = false;
                    break;
                }
                else
                {
                    if (!mKeyboardLetters[i].GetComponent<Button>().interactable)
                        mKeyboardLetters[i].GetComponent<Button>().interactable = true;
                }
            }
        }
    }
    [ContextMenu("StartAnimation")]
    public void DebugTest()
    {
        StartCoroutine(CountDownAnimation());
    }
    IEnumerator LockImage()
    {
        if (mIsLock)
        {
            while (lockImage.fillAmount < 1)
            {
                lockImage.fillAmount += 0.1f;
                yield return new WaitForSecondsRealtime(lockSpeed);
            }

        }
        else
        {
            while (lockImage.fillAmount > 0.5)
            {
                lockImage.fillAmount -= 0.1f;
                yield return new WaitForSecondsRealtime(lockSpeed);
            }
        }
        yield return null;
    }
    IEnumerator CountDownAnimation()
    {
        float animationTime = lockSpeed;
        while (animationTime > 0)
        {
            animationTime -= Time.deltaTime;
            lockImage.fillAmount = animationTime / lockSpeed;
        }
        yield return null;
    }
    public string OnCheckAnswer()
    {
        string tmpAnswer = "";
        for (int i = 0; i < GlobalParameters.lengthList; i++)
        {
            tmpAnswer += mTextsInput[i].transform.GetChild(1).GetComponent<Text>().text;
        }

        return tmpAnswer;

    }
}
