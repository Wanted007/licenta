﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class ToggleBlockFunctionality : MonoBehaviour
{

    public InputField[] inputs = null;
    InputLogic inputFunctionality = null;
    private Color oldColor;
    public Color colorSelected;
    public Image lockImage;
    public float speedLock = 0.1f;
    public Text infoText;
    bool isLock = true;

    void Start()
    {
        inputFunctionality = InputLogic.GetInstance();
        inputs = inputFunctionality.orderedInputFields;
    }

    void FixedUpdate()
    {
        
        if (inputFunctionality.inputFieldSelected.readOnly)
        {
            infoText.text = "Unlock";
            isLock = true;
            StartCoroutine("LockImage");

        }
        else
        {
            isLock = false;
            infoText.text = "Lock";
            StartCoroutine("LockImage");
        }
    }

    IEnumerator LockImage()
    {
        if (isLock)
        {
            while (lockImage.fillAmount < 1)
            {
                lockImage.fillAmount += 0.1f;
                yield return new WaitForSecondsRealtime(speedLock);
            }
        }
        else
        {
            while (lockImage.fillAmount > 0.5)
            {
                lockImage.fillAmount -= 0.1f;
                yield return new WaitForSecondsRealtime(speedLock);
            }
        }
        yield return null;
    }
    public void BlockInputField()
    {
        try
        {
            //this.GetComponent<Toggle>().isOn = !inputFunctionality.inputFieldSelected.readOnly;
            //if (inputFunctionality.inputFieldSelected.readOnly)
            //{
            //    this.GetComponent<Toggle>().isOn
            //}

            inputFunctionality.inputFieldSelected.readOnly = !inputFunctionality.inputFieldSelected.readOnly;

            if (inputFunctionality.inputFieldSelected.readOnly)
            {
                oldColor = inputFunctionality.inputFieldSelected.gameObject.GetComponent<Image>().color;
                inputFunctionality.inputFieldSelected.gameObject.GetComponent<Image>().color = new Color(0f, 0f, 0f, 1f);
                inputFunctionality.inputFieldSelected.selectionColor = new Color(0f, 0f, 0f, 0f);
            }
            else
            {
                inputFunctionality.inputFieldSelected.gameObject.GetComponent<Image>().color = oldColor;
                inputFunctionality.inputFieldSelected.selectionColor = colorSelected;
            }
            if (inputFunctionality.inputFieldSelected.readOnly)
                EventSystem.current.SetSelectedGameObject(inputFunctionality.inputFieldSelected.gameObject);
            else
                EventSystem.current.SetSelectedGameObject(inputFunctionality.orderedInputFields[++inputFunctionality.indexInputField].gameObject);
        }
        catch (Exception exception)
        {
            Debug.Log(exception.Message);

        }
    }
}
