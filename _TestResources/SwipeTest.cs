﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SwipeTest : MonoBehaviour
{

    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;
    Camera cameraMain;
    public bool debugMode = false;
    public float timeSwipe = 0.5f;
    public GameObject[] pages;
    public Image blackScreen;
    public float fadeTime;

    public Button nextBtn;
    public Button prevBtn;
    int contorPages = 0;
    void Start()
    {
        cameraMain = Camera.main;
    }
    void Update()
    {
        //OnCheckButton();

        if (Input.GetMouseButtonDown(0))
        {
            //save began touch 2d point
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(0))
        {
            //save ended touch 2d point
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            //create vector from the two points
            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);

            //normalize the 2d vector
            currentSwipe.Normalize();

            //swipe upwards
            if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            {
                //Debug.Log("up swipe");
            }
            //swipe down
            if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
            {
                //Debug.Log("down swipe");
            }

            
            //swipe left
            if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            {
                OnNextButton();

            }
            //swipe right
            if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
            {
                OnPrevButton();

                
            }
        }
    }
    //public IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
    //{

    //    float i = 0, rate = 1 / time;

    //    while (Vector3.Distance(thisTransform.position, endPos) >= .01f)
    //    {
    //        i += Time.deltaTime * rate;
    //        thisTransform.position = Vector3.Lerp(startPos, endPos, i);

    //        if (Vector3.Distance(thisTransform.position, endPos) <= .01f)
    //        {
    //            thisTransform.position = endPos;

    //            //ClassParameters.DisableInteraction = false;


    //            //this.GetComponent<SwipeTest>().enabled = false;
    //            //Debug.Log("Nume:" + this.gameObject.transform.parent.name);
    //            yield break;
    //        }

    //        //yield return ClassParameters.DisableInteraction = true;
    //    }

    //}

    public IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
    {

        //thisTransform.position = Vector3.Lerp(startPos, endPos, time);

        var i = 0.0f;
        var rate = 1.0f/ time;
        while (i < 1.0)
        {
            i += Time.deltaTime * rate;
            thisTransform.position = Vector3.Lerp(startPos, endPos, i);
            yield return null;
        }
        yield return null;
    }

    [ContextMenu("FadeToBlack")]
    public void FadeToBlack()
    {
        //blackScreen.color = Color.black;
        blackScreen.canvasRenderer.SetAlpha(0.0f);
        blackScreen.CrossFadeAlpha(1.0f, fadeTime, false);
    }

    [ContextMenu("FadeFromBlack")]  
    public void FadeFromBlack()
    {
        //blackScreen.color = Color.black;
        blackScreen.canvasRenderer.SetAlpha(1.0f);
        blackScreen.CrossFadeAlpha(0.0f, fadeTime, false);
    }

    public void OnNextButton()
    {
        if (contorPages < pages.Length - 1)
        {
            ++contorPages;
            //Debug.Log("ContorPages:" + contorPages);
            blackScreen = pages[contorPages].transform.GetChild(0).GetComponent<Image>();
            FadeToBlack();
            StartCoroutine(MoveObject(cameraMain.transform, cameraMain.transform.position, new Vector3(pages[contorPages].transform.position.x, pages[contorPages].transform.position.y, pages[contorPages].transform.position.z - 100), timeSwipe));
        }
        OnCheckButton();
       
    }
    public void OnPrevButton()
    {
        if (contorPages > 0)
        {
            --contorPages;
            blackScreen = pages[contorPages].transform.GetChild(0).GetComponent<Image>();
            FadeToBlack();
            StartCoroutine(MoveObject(cameraMain.transform, cameraMain.transform.position, new Vector3(pages[contorPages].transform.position.x, pages[contorPages].transform.position.y, pages[contorPages].transform.position.z - 100), timeSwipe));
        }
        OnCheckButton();
        
    }
    public void OnCheckButton()
    {
        if (contorPages == 0)
            prevBtn.interactable = false;
        else if (contorPages != 0 && !prevBtn.interactable)
            prevBtn.interactable = true;


        if (contorPages >= pages.Length - 1)
            nextBtn.interactable = false;
        else if (!nextBtn.interactable && contorPages < pages.Length - 1)
            nextBtn.interactable = true;

    }
    public void OnMainMenuScene()
    {
        SceneManager.LoadScene("StartScene");
    }
}
