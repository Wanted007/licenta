﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TestInputSelect : MonoBehaviour, ISelectHandler
{
    InputLogic inputLogic = null;
    public static bool canGoForward = false;
    public bool block = false;


    void Start()
    {
        inputLogic = InputLogic.GetInstance();
    }
    public void OnSelect(BaseEventData eventData)
    {
        try
        {
            inputLogic.inputFieldSelected = GetComponent<InputField>();

            if (canGoForward)
            {

                int position = this.transform.GetSiblingIndex();
                inputLogic.ChecksSelectedPosition(position);
            }
        }
        catch (Exception exception)
        {
            Debug.Log(exception.Message);
        }

    }
}
