﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Performance : MonoBehaviour
{


    public static readonly string BonusDayKey = "BonusDay";
    public static readonly string BonusWeekKey = "BonusWeek";
    public static readonly string BonusMonthKey = "BonusMonth";

    public static readonly string DayKey = "Day";
    public static readonly string WeekKey = "Week";
    public static readonly string MonthKey = "Month";

    public static readonly string YearKey = "Year";

    public static readonly string BonusLastDayPlayedKey = "BonusLastDayPlayed";
    public static readonly string BonusLastWeekPlayedKey = "BonusLastWeekPlayed";
    public static readonly string BonusLastMonthPlayedKey = "BonusLastMonthPlayed";

    public static readonly string LastDayPlayedKey = "LastDayPlayed";
    public static readonly string LastWeekPlayedKey = "LastWeekPlayed";
    public static readonly string LastMonthPlayedKey = "LastMonthPlayed";



    public static readonly string TotalBonus = "TotalBonus";
    public static readonly string TotalGamesPlayed = "TotalGamesPlayed";
    public static readonly string GamesLettersTotal = "GamesLettersTotal";
    public static readonly string GamesLettersLost = "GamesLettersLost";
    public static readonly string GamesNumbersTotal = "GamesNumbersTotal";
    public static readonly string GamesNumbersLost = "GamesNumbersLost";
    public static readonly string GamesAlphaTotal = "GamesAlphaTotal";
    public static readonly string GamesAlphaLost = "GamesAlphaLost";

    public static readonly string BestScoreAllGames = "BestScoreAllGames";
    public static readonly string BestScoreAllPercentage = "BestScoreAllPercentage";


}
