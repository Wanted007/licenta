﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioControl : MonoBehaviour {

    AudioManager audioManager = null;
    public Image settingsImage;

    void Start()
    {
        audioManager = AudioManager.GetInstance();
        settingsImage.sprite = (audioManager.clickSoundSource.enabled == true) ? audioManager.volumeOn : audioManager.volumeOff;
    }
    void OnEnable()
    {
       
    }
    public void OnLose()
    {
        audioManager.OnLose();
    }
    public void OnMute(Image _image)
    {
        audioManager.settingsImage = _image;
        audioManager.OnMute();
    }
    public void OnWin()
    {
        audioManager.OnWin();
    }
}
