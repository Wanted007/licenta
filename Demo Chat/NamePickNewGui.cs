﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof (ChatNewGui))]
public class NamePickNewGui : MonoBehaviour
{
	private const string UserNamePlayerPref = "NamePickUserName";
	
	public ChatNewGui chatNewComponent;
	
	public InputField idInput;
	
	public void Start()
	{
		this.chatNewComponent = FindObjectOfType<ChatNewGui>();

        Debug.Log("StartChat");
        StartChat();
	}
	
	
	// new UI will fire "EndEdit" event also when loosing focus. So check "enter" key and only then StartChat.
	public void EndEditOnEnter()
	{
		if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter))
		{
			this.StartChat();
		}
	}
	
	public void StartChat()
	{
		ChatNewGui chatNewComponent = FindObjectOfType<ChatNewGui>();
        Debug.Log("Ne conectăm pe numele:" + PhotonNetwork.playerName);
		chatNewComponent.UserName = PhotonNetwork.playerName;
		chatNewComponent.Connect();
		enabled = false;
		
		PlayerPrefs.SetString(NamePickNewGui.UserNamePlayerPref, chatNewComponent.UserName);
	}
}