﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamificationManager : MonoBehaviour {

    [Tooltip("Keyword used in PlayerPrefs. Must be the same with the one used in RandomNumber.cs Script")]
    public string GamesPlayedKeyPlayerPrefs = "GamesPlayed";
    public Image gamesPlayedImage;
    
    void OnEnable()
    {
        OnCalculatePerformanceGamesPlayed();
    }

    void OnCalculatePerformanceGamesPlayed()
    {
        if (PlayerPrefs.HasKey(GamesPlayedKeyPlayerPrefs))
        {
            float gamesPlayed = PlayerPrefs.GetInt(GamesPlayedKeyPlayerPrefs);
            gamesPlayedImage.fillAmount = gamesPlayed / 10;
        }
    }

    [ContextMenu("ResetPerformance")]
    public void OnResetPerformance()
    {
        PlayerPrefs.DeleteAll();
    }

}
