﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Animations : MonoBehaviour
{

    public RectTransform imageRef;
    public GameObject test;

    public bool leftAnimation = false;
    public IEnumerator LeftAnimation()
    {
        if (imageRef.anchorMin.x>0)
        {
            imageRef.anchorMin -= new Vector2(0.1f, 0);
            imageRef.offsetMax = Vector2.zero;
            imageRef.offsetMin = Vector2.zero;
            
        }
        yield return new WaitForSeconds(0.5f);
        //yield return null;
    }
    public void OnDoSomething()
    {
        leftAnimation = true;
    }

    void Update()
    {
        if (leftAnimation)
        {
            if (imageRef.anchorMin.x > 0)
            {
                imageRef.anchorMin -= new Vector2(0.005f, 0);
                imageRef.offsetMax = Vector2.zero;
                imageRef.offsetMin = Vector2.zero;

            }
            else
                leftAnimation = false;
        }
    }

}
