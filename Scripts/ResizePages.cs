﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResizePages : MonoBehaviour
{

    public RectTransform[] Pages;

    private Camera camera;

    void Start()
    {
        camera = GetComponent<Camera>();
        ResizeAllPages();
    }

    void ResizeAllPages()
    {
        for (int i = 0; i < Pages.Length; i++)
        {
            Pages[i].sizeDelta = new Vector2(Pages[i].rect.height * camera.aspect, Pages[i].rect.height);
        }
    }

}
