﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System.Net;
//using System.Net.Sockets;
//using UnityEngine;

//public class Server : MonoBehaviour
//{

//    public int port = 6321;

//    private List<ServerClient> clients;
//    private List<ServerClient> disconnectList;

//    private TcpListener server;
//    private bool serverStarted;

//    bool isPlayer2 = false;

//    public void Init()
//    {
//        DontDestroyOnLoad(gameObject);
//        clients = new List<ServerClient>();
//        disconnectList = new List<ServerClient>();

//        try
//        {
//            server = new TcpListener(IPAddress.Any, port);
//            server.Start();

//            StartListening();
//            serverStarted = true;
//        }
//        catch (Exception exception)
//        {
//            Debug.Log("Socket error:" + exception.Message);
//        }

//    }

//    void Update()
//    {
//        if (!serverStarted)
//            return;

//        foreach (ServerClient client in clients)
//        {
//            if (!IsConnected(client.tcp))
//            {
//                client.tcp.Close();
//                disconnectList.Add(client);
//                continue;
//            }
//            else
//            {
//                NetworkStream stream = client.tcp.GetStream();
//                if (stream.DataAvailable)
//                {
//                    StreamReader streamReader = new StreamReader(stream, false);
//                    string data = streamReader.ReadLine();
//                    if (data != null)
//                        OnIncomingData(client, data);
//                }
//            }
//        }
//        for (int i = 0; i < disconnectList.Count-1; i++)
//        {
//            clients.Remove(disconnectList[i]);
//            disconnectList.RemoveAt(i);
//        }
//        if(isPlayer2)
//        {
//            GameManager.Instance.OnChangeScene();
//            isPlayer2 = false;
//        }
//    }

//    //Server Send
//    private void Broadcast(string _data, List<ServerClient> _clients)
//    {
//        foreach (ServerClient tmpClient in _clients)
//        {
//            try
//            {
//                StreamWriter writer = new StreamWriter(tmpClient.tcp.GetStream());
//                writer.WriteLine(_data);
//                writer.Flush();

//            }
//            catch (Exception exception)
//            {
//                Debug.Log(exception.Message);
//            }
//        }
//    }

//    private void Broadcast(string _data, ServerClient _clients)
//    {
//        List<ServerClient> tmpClients = new List<ServerClient>() { _clients };
//        Broadcast(_data, tmpClients);
//    }
//    //Server Read
//    private void OnIncomingData(ServerClient _client, string _data)
//    {
//        Debug.Log(_client.clientName+" : "+_data);
//    }

//    private bool IsConnected(TcpClient _client)
//    {
//        try
//        {
//            if (_client != null && _client.Client != null && _client.Client.Connected)
//            {
//                if (_client.Client.Poll(0, SelectMode.SelectRead))
//                    return !(_client.Client.Receive(new byte[1], SocketFlags.Peek) == 0);

//                return true;
//            }
//            else return false;
//        }
//        catch (Exception exception)
//        {
//            return false;
//        }

//    }
//    void StartListening()
//    {
//        server.BeginAcceptTcpClient(AcceptTcpClient, server);
//    }
//    void AcceptTcpClient(IAsyncResult _asyncResult)
//    {
//        TcpListener listener = (TcpListener)_asyncResult.AsyncState;
//        string allUsers = "";
//        foreach (var tmpServerClient in clients)
//        {
//            allUsers += tmpServerClient.clientName + "|";
//        }
//        ServerClient serverClient = new ServerClient(listener.EndAcceptTcpClient(_asyncResult));
//        clients.Add(serverClient);

//        StartListening();
//        Debug.Log("Somebody has connected");
//        isPlayer2 = true;
        



//        Broadcast("SWHO|"+allUsers, clients[clients.Count - 1]);

//    }
//}

