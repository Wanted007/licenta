﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using UnityEngine;

public class Client : MonoBehaviour {

    public string clientName;
    private bool isSocketReady;
    private TcpClient socket;
    private NetworkStream stream;
    private StreamWriter streamWriter;
    private StreamReader streamReader;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
    public bool ConnectToServer(string _host, int _port)
    {
        if (isSocketReady)
            return false;

        try
        {
            socket = new TcpClient(_host, _port);
            stream = socket.GetStream();
            streamWriter = new StreamWriter(stream);
            streamReader = new StreamReader(stream);

            isSocketReady = true;

        }
        catch (Exception exception)
        {
            Debug.Log(exception.Message);
        }

        return isSocketReady;
    }

    void Update()
    {
        if (isSocketReady)
        {
            if (stream.DataAvailable)
            {
                string tmpData = streamReader.ReadLine();
                if (tmpData != null)
                    OnIncomingData(tmpData);

            }
        }
    }
    //Send messages to the server
    public void Send(string _data)
    {
        if (!isSocketReady)
            return;

        streamWriter.WriteLine(_data);
        streamWriter.Flush();
    }
    //Read messages from server
    private void OnIncomingData(string _data)
    {
        Debug.Log(_data);
        string[] tmpData = _data.Split('|');

    }

    void CloseSocket()
    {
        if (!isSocketReady)
            return;

        streamWriter.Close();
        streamReader.Close();
        socket.Close();
        isSocketReady = false;
    }

    void OnApplicationQuit()
    {
        CloseSocket();

    }
    void OnDisable()
    {
        CloseSocket();
    }

}
