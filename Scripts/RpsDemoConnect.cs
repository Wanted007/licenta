﻿using Photon;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using ExitGames.Client.Photon;

public class RpsDemoConnect : PunBehaviour
{
    public InputField userNameInputField;
    public InputField codeForOponent;
    public InputField mRoomNameInputField;
    public InputField passwordRoomInputField;
    public string UserId;

    string previousRoomPlayerPrefKey = "PUN:Demo:RPS:PreviousRoom";
    public string previousRoom;

    private const string MainSceneName = "DemoRPS-Scene";

    const string NickNamePlayerPrefsKey = "NickName";
    public GameObject mLoadingPanel;
    public Text errorMessage;
    string CODE_OPPONENT_KEY = "CodeOpponent";
    string errorFieldsValidate;
    string PASSWORD_ROOM_KEY = "Password";

    public GameObject prefabRoomName;
    public GameObject parentContainerRoomName;
    public Image panelRefreshFade;
    public GameObject connectRoomPanel;
    public GameObject createRoomPanel;
    public float fadeTime = 1f;
    public Color32 colorUnCheckedToggle;
    public Color32 colorCheckedToggle = Color.white;
    public Button connectRoomBtn;

    //For join room
    public InputField userNameInputFieldJoin;
    public InputField codeForOponentJoin;
    public InputField passwordRoomInputFieldJoin;
    public GameObject chatPanel;
    void Start()
    {
        userNameInputField.text = PlayerPrefs.HasKey(NickNamePlayerPrefsKey) ? PlayerPrefs.GetString(NickNamePlayerPrefsKey) : "";
    }


    public void ApplyUserIdAndConnect()
    {
        mLoadingPanel.SetActive(true);
        string nickName = "DemoNick";
        if (this.userNameInputField != null && !string.IsNullOrEmpty(this.userNameInputField.text))
        {
            nickName = this.userNameInputField.text;
            PlayerPrefs.SetString(NickNamePlayerPrefsKey, nickName);
        }
        //if (string.IsNullOrEmpty(UserId))
        //{
        //    this.UserId = nickName + "ID";
        //}
        //else
        //{
        //    Debug.Log("Re-using AuthValues. UserId: " + PhotonNetwork.AuthValues.UserId);
        //}


        PhotonNetwork.AuthValues = new AuthenticationValues();

        PhotonNetwork.AuthValues.UserId = userNameInputField.text;
        //PhotonNetwork.AuthValues.AddAuthParameter(PASSWORD_ROOM_KEY, passwordRoomInputField.text);
        //PhotonNetwork.AuthValues.AddAuthParameter(CODE_OPPONENT_KEY, codeForOponent.text);





        PhotonNetwork.player.NickName = nickName;
        ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable();
        hashTable.Add(CODE_OPPONENT_KEY, codeForOponent.text);
        hashTable.Add("PlayerName", userNameInputField.text);
        if (!string.IsNullOrEmpty(passwordRoomInputField.text))
        {
            hashTable.Add(PASSWORD_ROOM_KEY, passwordRoomInputField.text);
        }
        PhotonNetwork.player.SetCustomProperties(hashTable);

        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 2 };

        if (PhotonNetwork.CreateRoom(mRoomNameInputField.text, roomOptions, TypedLobby.Default))
        {
            Debug.Log("create room successfully sent:"+ mRoomNameInputField.text);
        }
        else
        {
            Debug.Log("create room failed to send");
        }

    }
    void Awake()
    {
        PhotonNetwork.ConnectUsingSettings("0.5");
        PhotonHandler.StopFallbackSendAckThread();
    }
    [ContextMenu("FadeToBlack")]
    public void FadeToBlack()
    {
        //blackScreen.color = Color.black;
        panelRefreshFade.canvasRenderer.SetAlpha(0.0f);
        panelRefreshFade.CrossFadeAlpha(1.0f, fadeTime, false);
    }

    [ContextMenu("FadeFromBlack")]
    public void FadeFromBlack()
    {
        //blackScreen.color = Color.black;
        panelRefreshFade.canvasRenderer.SetAlpha(1.0f);
        panelRefreshFade.CrossFadeAlpha(0.0f, fadeTime, false);
    }
    public void OnClickRoom(Toggle _toggle)
    {
        FadeToBlack();
        connectRoomPanel.SetActive(_toggle.isOn);
        createRoomPanel.SetActive(!_toggle.isOn);
        _toggle.image.color = (_toggle.isOn == true) ? colorCheckedToggle : colorUnCheckedToggle;

        connectRoomBtn.onClick.RemoveAllListeners();
        connectRoomBtn.onClick.AddListener(() => OnJoinExistentRoom(_toggle.transform.GetChild(0).GetComponent<Text>()));

    }
    public override void OnReceivedRoomListUpdate()
    {
        //1. Find out all rooms available
        RoomInfo[] rooms = PhotonNetwork.GetRoomList();

        //2. Destroy all rooms.
        int tmpChildren = parentContainerRoomName.transform.childCount;
        for (int i = tmpChildren - 1; i > 0; i--)
        {
            GameObject.Destroy(parentContainerRoomName.transform.GetChild(i).gameObject);
        }

        //3. Instantiate all roooms available
        foreach (RoomInfo room in rooms)
        {
            GameObject tmpBtn = Instantiate(prefabRoomName, parentContainerRoomName.transform) as GameObject;
            tmpBtn.transform.GetChild(0).GetComponent<Text>().text = room.Name;
            //tmpBtn.transform.GetComponent<Button>().onClick.AddListener(()=>OnClickRoom(GetComponent<Toggle>()));
            tmpBtn.transform.GetComponent<Toggle>().onValueChanged.AddListener((value) => OnClickRoom(tmpBtn.transform.GetComponent<Toggle>()));
            tmpBtn.transform.GetComponent<Toggle>().group = parentContainerRoomName.GetComponent<ToggleGroup>();
            
        }

    }
    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster");
        // after connect 
        this.UserId = PhotonNetwork.player.UserId;
        ////Debug.Log("UserID " + this.UserId);

        if (PlayerPrefs.HasKey(previousRoomPlayerPrefKey))
        {

            this.previousRoom = PlayerPrefs.GetString(previousRoomPlayerPrefKey);
            PlayerPrefs.DeleteKey(previousRoomPlayerPrefKey); // we don't keep this, it was only for initial recovery
        }


        // after timeout: re-join "old" room (if one is known)
        //if (!string.IsNullOrEmpty(this.previousRoom))
        //{

        //    PhotonNetwork.ReJoinRoom(this.previousRoom);
        //    this.previousRoom = null;       // we only will try to re-join once. if this fails, we will get into a random/new room
        //}
        //else
        //{
        // else: join a random room
        //nu sunt de acord
        //PhotonNetwork.JoinRandomRoom();
        //}
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }
    public void OnJoinExistentRoom(Text _text)
    {
        //PhotonNetwork.JoinRoom(_text.text);
        //PhotonNetwork.JoinRoom("Chelsea");
        PhotonNetwork.player.NickName = userNameInputFieldJoin.text;
        ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable();
        hashTable.Add(CODE_OPPONENT_KEY, codeForOponentJoin.text);
        hashTable.Add("PlayerName", userNameInputFieldJoin.text);
        if (!string.IsNullOrEmpty(passwordRoomInputFieldJoin.text))
        {
            hashTable.Add(PASSWORD_ROOM_KEY, passwordRoomInputFieldJoin.text);
        }
        PhotonNetwork.player.SetCustomProperties(hashTable);
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = 2 };
        PhotonNetwork.JoinOrCreateRoom(_text.text, roomOptions, TypedLobby.Default);
        Debug.Log("Intram in:" + _text.text);
        //if (PhotonNetwork.room.CustomProperties.ContainsKey(PASSWORD_ROOM_KEY))
        //{
        //    string password = PhotonNetwork.room.CustomProperties[PASSWORD_ROOM_KEY].ToString();
        //    if (password != passwordRoomInputField.text)
        //        PhotonNetwork.LeaveRoom();
        //}

        //ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable();
        //hashTable.Add("PlayerName", PhotonNetwork.player.NickName);
        ////hashTable.Add(PASSWORD_ROOM_KEY, passwordRoomInputField.text);
        //hashTable.Add(CODE_OPPONENT_KEY, codeForOponent.text);
        //PhotonNetwork.player.SetCustomProperties(hashTable);

        //PhotonNetwork.AuthValues.
    }
    public override void OnJoinedLobby()
    {
        Debug.Log("OnJoinLobby");
        //var roomList = PhotonNetwork.GetRoomList();
        //Debug.Log("List:" + roomList.Length);
        //Debug.Log("Am intrat");
        ////Debug.Log("RoomName:" + PhotonNetwork.room.Name);
        //foreach (var item in roomList)
        //{
        //    Debug.Log("Exista:" + item.Name);
        //}
        /* OnConnectedToMaster();*/ // this way, it does not matter if we join a lobby or not
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        Debug.Log("OnPhotonRandomJoinFailed");
        PhotonNetwork.CreateRoom(mRoomNameInputField.text, new RoomOptions() { MaxPlayers = 2, PlayerTtl = 20000 }, null);
    }


#if UNITY_EDITOR
    [ContextMenu("ShowRoom")]
#endif
    public void DebugTest()
    {
        //Debug.Log("Conectat:"+PhotonNetwork.connected);

        //Debug.Log(PhotonNetwork.connectionStateDetailed.ToString());

        //var roomList=PhotonNetwork.GetRoomList();
        //Debug.Log("List:" + roomList.Length);
        ////Debug.Log("RoomName:" + PhotonNetwork.room.Name);
        //foreach (var item in roomList)
        //{
        //    Debug.Log("Exista:" + item.Name);
        //}
        Debug.Log("NrPlayeri:"+PhotonNetwork.room.PlayerCount);
    }
    public void OnCreateRoom()
    {
        if (!string.IsNullOrEmpty(userNameInputField.text) && !string.IsNullOrEmpty(mRoomNameInputField.text) && !string.IsNullOrEmpty(codeForOponent.text))
        {
            PhotonNetwork.AuthValues = new AuthenticationValues();
            PhotonNetwork.AuthValues.UserId = userNameInputField.text;
            PhotonNetwork.AuthValues.AddAuthParameter(PASSWORD_ROOM_KEY, passwordRoomInputField.text);
            PhotonNetwork.AuthValues.AddAuthParameter(CODE_OPPONENT_KEY, codeForOponent.text);

            RoomOptions roomOptions = new RoomOptions()
            {
                IsVisible = true,
                IsOpen = string.IsNullOrEmpty(passwordRoomInputField.text),
                MaxPlayers = 2

            };
            roomOptions.CustomRoomProperties.Add(PASSWORD_ROOM_KEY, passwordRoomInputField.text);
            if (PhotonNetwork.CreateRoom(mRoomNameInputField.text, roomOptions, TypedLobby.Default))
            {
               
                mLoadingPanel.SetActive(true);
                Debug.Log("create room successfully");
            }
            else
            {
                Debug.Log("create room failed");

            }
        }
        else
        {
            errorMessage.text = errorFieldsValidate;
        }
    }
    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
        //this.previousRoom = PhotonNetwork.room.Name;
        //PlayerPrefs.SetString(previousRoomPlayerPrefKey, this.previousRoom);

        //if (PhotonNetwork.room.CustomProperties.ContainsKey(PASSWORD_ROOM_KEY))
        //{
        //    string password = PhotonNetwork.room.CustomProperties[PASSWORD_ROOM_KEY].ToString();
        //    if (password != passwordRoomInputField.text)
        //        PhotonNetwork.LeaveRoom();
        //}

        //ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable();
        //hashTable.Add("PlayerName", PhotonNetwork.player.NickName);
        ////hashTable.Add(PASSWORD_ROOM_KEY, passwordRoomInputField.text);
        //hashTable.Add(CODE_OPPONENT_KEY, codeForOponent.text);
        //PhotonNetwork.player.SetCustomProperties(hashTable);

        chatPanel.SetActive(true);

    }

    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        Debug.Log("OnPhotonRandomJoinRoomFailed");
        //PhotonNetwork.JoinRoom("CasaMare");
        this.previousRoom = null;
        PlayerPrefs.DeleteKey(previousRoomPlayerPrefKey);
    }

    public override void OnConnectionFail(DisconnectCause cause)
    {
        Debug.Log("Disconnected due to: " + cause + ". this.previousRoom: " + this.previousRoom);
    }

    public override void OnPhotonPlayerActivityChanged(PhotonPlayer otherPlayer)
    {
        Debug.Log("OnPhotonPlayerActivityChanged() for " + otherPlayer.NickName + " IsInactive: " + otherPlayer.IsInactive);
    }

}
