﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlphaAnimation : MonoBehaviour
{

    public GameObject[] images;
    public GameObject[] texts;

    public GameObject yourImage=null;
    public GameObject yourText=null;

    public bool fadeIn = false;
    public bool fadeOut = false;
    public float speedTime = 5f;
    public float timeBeginFade = 3f;
    public float incrementAlphaTime = 0.05f;
    IEnumerator OnIncreaseAlpha()
    {

        yield return null;
    }

    IEnumerator OnDecreaseAlpha()
    {
        yield return null;
    }

    [ContextMenu("StartFadeIn")]
    public void OnStartFadeIn()
    {
        StartCoroutine(ImageFadeTo(0f, speedTime));
        Debug.Log("Apelat");
    }
    void Start()
    {
        StartCoroutine(ImageFadeFrom(0f, speedTime));
        StartCoroutine(TextFadeFrom(0f, speedTime));
    }


    IEnumerator ImageFadeTo(float aValue, float aTime)
    {
        foreach (GameObject image in images)
        {

            Color imageColor = image.GetComponent<Image>().color;

            float alpha = imageColor.a;
            Debug.Log(alpha);
            while (alpha > 0.0f)
            {
                alpha -= 0.075f;
                imageColor.a = alpha;
                image.GetComponent<Image>().color = imageColor;
                yield return new WaitForSecondsRealtime(0.05f);
            }
        }


        yield return null;
    }

    IEnumerator TextFadeTo(float aValue, float aTime)
    {
        foreach (GameObject text in texts)
        {

            Color imageColor = text.GetComponent<Text>().color;

            float alpha = imageColor.a;
            Debug.Log(alpha);
            while (alpha > 0.0f)
            {
                alpha -= 0.075f;
                imageColor.a = alpha;
                text.GetComponent<Text>().color = imageColor;
                yield return new WaitForSecondsRealtime(0.05f);
            }
        }


        yield return null;
    }

    IEnumerator ImageFadeFrom(float aValue, float aTime)
    {
        yield return new WaitForSecondsRealtime(timeBeginFade);
        if (yourImage != null)
        {

            Color imageColor = yourImage.GetComponent<Image>().color;

            float alpha = imageColor.a;
            while (alpha < 1.0f)
            {
                alpha += 0.075f;
                imageColor.a = alpha;
                yourImage.GetComponent<Image>().color = imageColor;
                yield return new WaitForSecondsRealtime(incrementAlphaTime);
            }
        }


        yield return null;
    }

    IEnumerator TextFadeFrom(float aValue, float aTime)
    {
        yield return new WaitForSecondsRealtime(timeBeginFade);

        if (yourText != null)
        {

            Color imageColor = yourText.GetComponent<Text>().color;

            float alpha = imageColor.a;
            while (alpha < 1.0f)
            {
                alpha += 0.075f;
                imageColor.a = alpha;
                yourText.GetComponent<Text>().color = imageColor;
                yield return new WaitForSecondsRealtime(incrementAlphaTime);
            }
        }

        yield return null;
    }
}
