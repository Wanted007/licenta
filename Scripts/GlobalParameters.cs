﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameType { Numeric, Letters, AlphaNumeric};
public class GlobalParameters {

    public static int lengthList=5;
	public static string currentCode="";
    public static string hintCode = "";
    public static List<string> hintsCode= new List<string>();
    public static string currentResult="";
	public static GameType gameType=GameType.Numeric;
    public static int lettersLength = 0;
}
