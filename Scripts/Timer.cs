﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public Text timerText;
    public float startTime;
    float time;
    string minutes = "";
    string seconds = "";

    void OnEnable()
    {
        startTime = Time.time;
    }

    void FixedUpdate()
    {
        time = Time.time - startTime;

        minutes = ((int)time / 60).ToString();
        seconds = ((int)time % 60).ToString();
        timerText.text = minutes + ":" + seconds;
    }

    [ContextMenu("ResetTime")]
    public void OnResetTimer()
    {
        timerText.text = "";
        startTime = 0;
        time = 0;
        minutes = "";
        seconds = "";
        startTime = Time.time;
    }

}
