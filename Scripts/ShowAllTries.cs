﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowAllTries : MonoBehaviour {

	public GameObject prefabObject;
	public GameObject parentScrollObject;


    //void OnEnable()
    //{
    //	OnInstantiateLastTry ();
    //}
    void OnEnable()
    {
        //OnResetElements();
    }
    [ContextMenu("DebugInfo")]
    public void OnShowInfo()
    {
        Debug.Log("Position:" + gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition);
        gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
    }
	public void OnInstantiateLastTry()
	{
        Debug.Log("GlobalParameters.currentResult:" + GlobalParameters.currentResult);
		string[] textResult = GlobalParameters.currentResult.Split (',');
        
        GameObject result = Instantiate(prefabObject, parentScrollObject.transform) as GameObject;
        result.transform.SetAsFirstSibling();
        
        
        result.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = textResult[1]+"X";
        result.transform.GetChild(1).GetChild(1).GetComponent<Text>().text = textResult[2]+"X";
        result.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = textResult[0];
        float scrollValue = gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition;
       
        StartCoroutine("UpdateScrollPosition");
        
    }
    
    IEnumerator UpdateScrollPosition()
    {
        gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
        yield return new WaitForSecondsRealtime(0.2f);
        gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
        yield return new WaitForSecondsRealtime(0.1f);
        gameObject.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
        yield return new WaitForSecondsRealtime(0.1f);

        yield return null;
    }
    [ContextMenu("OnDestroyElements")]
    public void OnResetElements()
    {
        for (int i = 0; i < parentScrollObject.transform.childCount; i++)
        {
            Destroy(parentScrollObject.transform.GetChild(i).gameObject);
        }
    }

}
