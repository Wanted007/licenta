﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonFunctionality : MonoBehaviour {

    public InputField input;
    public int nrLimit;

    public  Functionality mref;

    public void OnMouseDown()
    {
        if (input.text.Length == (nrLimit - 1))
        {
            OnClick();
            mref.curentBtn = mref.okBtn;
            mref.OnResetButton();
        }
        else if (input.text.Length < nrLimit)
        {
            OnClick();

        }
        
    }

    public void OnClick()
    {
        string number = this.gameObject.transform.GetChild(0).GetComponent<Text>().text;
        input.text += number;

        if (Functionality.startState)
        {

            mref.OnResetButton();
            Functionality.startState = false;
        }

        mref.curentBtn = this.gameObject;
        mref.OnClickButton();
    }
}
