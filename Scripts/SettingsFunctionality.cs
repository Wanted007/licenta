﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SettingsFunctionality : MonoBehaviour {

	/*
     * -Avem Volum?
     */


	public int[] charactersLength=new int[]{5,6,7,8,9,10};
	GameType gameType;

	public Dropdown charactersLengthDropDown;
	public Dropdown levelSelectDropDown;

	void InitializeDropDowns()
	{
		//CharactersLength
		List<Dropdown.OptionData> listaCharactersLength = new List<Dropdown.OptionData> ();
		foreach (var item in charactersLength) {
			listaCharactersLength.Add (new Dropdown.OptionData(item.ToString ()));
		}
		charactersLengthDropDown.options = listaCharactersLength;


		//Level Select
		List<Dropdown.OptionData> listaLevelSelect = new List<Dropdown.OptionData> () {
		
			new Dropdown.OptionData(GameType.AlphaNumeric.ToString()),
			new Dropdown.OptionData(GameType.Letters.ToString()),
			new Dropdown.OptionData(GameType.Numeric.ToString())

		};
		levelSelectDropDown.options = listaLevelSelect;



	}

	void Awake(){
	
		InitializeDropDowns ();
	}

	[ContextMenu("SaveSettings")]
	public void OnSaveSettings()
	{
		GlobalParameters.lengthList=charactersLength[charactersLengthDropDown.value];
		switch (levelSelectDropDown.value) {

		case 0:
			GlobalParameters.gameType = GameType.AlphaNumeric;
			break;
		case 1:
			GlobalParameters.gameType = GameType.Letters;
			break;
		case 2:
			GlobalParameters.gameType = GameType.Numeric;
			break;

		default:
			break;
		}

        

	}

	
}
