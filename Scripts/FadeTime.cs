﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public static class GraphicExtensions
{
    /// <summary>
    /// Fade methods forUI elements;
    /// </summary>
    /// <param name="g"></param>
    public static void FadeIn(this Graphic _text)
    {
        _text.GetComponent<CanvasRenderer>().SetAlpha(0f);
        _text.CrossFadeAlpha(1f, .15f, false);//second param is the time
    }
    public static void FadeOut(this Graphic _text)
    {
        _text.GetComponent<CanvasRenderer>().SetAlpha(1f);
        _text.CrossFadeAlpha(0f, .15f, false);
    }
}
public class FadeTime : MonoBehaviour {

    public Vector3 mDesiredScale;
    Vector3 mNormalScale = Vector3.one;
    public float mTimeAnimation = 0.05f;

	// Use this for initialization
	void Start () {
        //gameObject.GetComponent<Graphic>().FadeIn();
        StartCoroutine(StartScaleAnimationTo(OnStartScaleFrom));
    }
	
	// Update is called once per frame
	void Update () {
        //gameObject.GetComponent<Graphic>().FadeIn();
        //gameObject.GetComponent<Graphic>().FadeOut();
        //transform.localScale = Vector3.Lerp(transform.localScale, mDesiredScale, mTimeAnimation);

        //if (transform.localScale != mDesiredScale)
        //{
            
        //        transform.localScale = Vector3.Lerp(transform.localScale, mDesiredScale, mTimeAnimation);
        //}
        //else
        //{
            
        //        transform.localScale = Vector3.Lerp(transform.localScale, mNormalScale, mTimeAnimation);
        //}
    }
    void OnStartScaleFrom()
    {
        StopAllCoroutines();
        StartCoroutine(StartScaleAnimationFrom());
    }
    IEnumerator StartScaleAnimationTo(Action _callback)
    {
        float elapsedTime = 0f;
        while(elapsedTime<mTimeAnimation)
        {
            transform.localScale = Vector3.Lerp(mNormalScale, mDesiredScale, (elapsedTime / mTimeAnimation));
            transform.GetComponent<CanvasGroup>().alpha = Mathf.Lerp(1f, 0.5f, (elapsedTime / mTimeAnimation));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        _callback();
    }
    IEnumerator StartScaleAnimationFrom()
    {
        float elapsedTime = 0f;
        while (elapsedTime < mTimeAnimation)
        {
            transform.localScale = Vector3.Lerp(mDesiredScale, mNormalScale, (elapsedTime / mTimeAnimation));
            transform.GetComponent<CanvasGroup>().alpha = Mathf.Lerp(0.5f, 1.0f, (elapsedTime / mTimeAnimation));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        StartCoroutine(StartScaleAnimationTo(OnStartScaleFrom));
    }
}
