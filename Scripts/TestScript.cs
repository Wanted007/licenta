﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestScript : MonoBehaviour
{

    public Text testText;
    public Text testText2;
    public static bool firstOne = true;
    string firstPlayer = "firstPlayer";
    string secondPlayer = "secondPlayer";
    string yourText = "";
    public Button btnTest;

    public void OnJoinedRoom()
    {
        //testText2.text = "JoinedRoom";
        if(PhotonNetwork.isMasterClient)
        {
            yourText = "firstPlayer";
        }
        else
        {
            yourText = "secondPlayer";
        }
        testText2.text = yourText;

    }
    void Update()
    {
        //testText.text = PhotonNetwork.isMasterClient.ToString();
    }

    public void OnDoAction()
    {
        if(firstOne)
        {
            
            testText.text += yourText;
            firstOne = false;
            
            //btnTest.interactable = false;

        }
        else
        {
            if (PhotonNetwork.isMasterClient)
                return;

                testText.text += yourText;
                firstOne = true;
        }
    }
}

