﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateAnimation : MonoBehaviour
{

    public GameObject[] objectsToAnimate;

	public bool resetPosition=false;
    void OnEnable()
    {
        StartCoroutine(Animation());
    }

    IEnumerator Animation()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        foreach (var item in objectsToAnimate)
        {
            item.SetActive(true);
            while (item.GetComponent<CanvasGroup>().alpha < 1)
            {
                item.GetComponent<CanvasGroup>().alpha += 0.1f;
                yield return new WaitForSecondsRealtime(0.05f);
            }
            yield return new WaitForSecondsRealtime(0.5f);
        }
        yield return null;
    }

    void OnDisable()
    {
		if (resetPosition) {
			foreach (var item in objectsToAnimate) {
				item.SetActive (false);
				item.GetComponent<CanvasGroup> ().alpha = 0f;

			}
		}
    }
}
