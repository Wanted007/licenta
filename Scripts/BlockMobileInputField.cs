﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMobileInputField : MonoBehaviour
{

    private TouchScreenKeyboard mKeyboardMobile;

    void Awake()
    {
        mKeyboardMobile = TouchScreenKeyboard.Open("text");
        mKeyboardMobile.active = false;
    }

    void Update()
    {
        mKeyboardMobile.active = false;
    }
}
