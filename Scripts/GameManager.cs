﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool isStartScene = true;

    
    public InputField multiplayerInput1;
    public InputField multiplayerInput2;
    GameManager instance = null;
    public GameObject startGame;

    public bool isWIP = false;

    void Awake()
    {
        
    
            if (instance == null)
                instance = this;
            else if (instance != this)
                Destroy(gameObject);


            DontDestroyOnLoad(gameObject);
        
    }
    public void OnValidateInput()
    {
        if (isStartScene)
        {
            

        }
        else
        {
            multiplayerInput1.characterLimit = GlobalParameters.lengthList;
            switch (GlobalParameters.gameType)
            {
                case GameType.AlphaNumeric:
                    multiplayerInput1.contentType = InputField.ContentType.Alphanumeric;
                    break;

                case GameType.Letters:
                    multiplayerInput1.contentType = InputField.ContentType.Name;
                    break;
                case GameType.Numeric:
                    multiplayerInput1.contentType = InputField.ContentType.IntegerNumber;
                    break;
                default:
                    break;
            }

            multiplayerInput2.characterLimit = GlobalParameters.lengthList;
            switch (GlobalParameters.gameType)
            {
                case GameType.AlphaNumeric:
                    multiplayerInput2.contentType = InputField.ContentType.Alphanumeric;
                    break;

                case GameType.Letters:
                    multiplayerInput2.contentType = InputField.ContentType.Name;
                    break;
                case GameType.Numeric:
                    multiplayerInput2.contentType = InputField.ContentType.IntegerNumber;
                    break;
                default:
                    break;
            }


        }
    }
    void Start()
    {
        if (!isWIP)
        {
            if (isStartScene)
                startGame.SetActive(true);
            OnValidateInput();
        }
    }

    public void OnChangeScene()
    {
        if (isStartScene)
        {
            SceneManager.LoadScene("MultiPlayerScene");
        }
        else
        {
            SceneManager.LoadScene("StartScene");
        }
    }

    public void OnCloseApplication()
    {
        Application.Quit();
    }
    public void OnLoadPerformanceScene()
    {
        SceneManager.LoadScene("PerformanceScene");
    }
}
