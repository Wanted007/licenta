﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Functionality : MonoBehaviour
{

    public Color startColor;
    public Color clickColor;

    public Color blackColor;
    public GameObject startBtn;
    public GameObject curentBtn;
    public GameObject okBtn;

    public GameObject[] allButtons;

    public InputField yourChoose;

    public int limitNumbers;

    public Text sourceText;

    public GameObject finishPanel;

    public static bool startState = true;
    List<int> allNumbers = new List<int>();
    string number;


    public void Init()
    {
        foreach (var item in allButtons)
        {
            item.GetComponent<Button>().enabled = true;
            item.GetComponent<Image>().color = startColor;
            item.transform.GetChild(0).GetComponent<Text>().color = clickColor;
        }


        curentBtn = okBtn;
        OnClickButton();
        curentBtn = startBtn;
        OnClickButton();
    }

    public void OnGenerateNumber()
    {
        int nr1 = Random.Range(1, 9);
        allNumbers.Add(nr1);

        while (allNumbers.Count < limitNumbers)
        {
            int nr = Random.Range(0, 9);
            if (!allNumbers.Contains(nr))
            {
                allNumbers.Add(nr);
            }
        }
        foreach (var item in allNumbers)
        {
            //Debug.Log(item);
        }
    }
    void Awake()
    {
        curentBtn = okBtn;
        OnClickButton();
        curentBtn = startBtn;
        OnClickButton();

        OnGenerateNumber();
    }

    public void OnClickButton()
    {

        curentBtn.GetComponent<Button>().enabled = false;
        curentBtn.GetComponent<Image>().color = clickColor;
        curentBtn.transform.GetChild(0).GetComponent<Text>().color = blackColor;

    }

    public void OnResetButton()
    {
        curentBtn.GetComponent<Button>().enabled = true;
        curentBtn.GetComponent<Image>().color = startColor;
        curentBtn.transform.GetChild(0).GetComponent<Text>().color = clickColor;
    }

    public int OnCheckCows()
    {
        int cows = 0;
        string result = yourChoose.text;
        int[] number = new int[result.Length];
        for (int i = 0; i < number.Length; i++)
        {
            number[i] = int.Parse(result[i].ToString());
        }
        foreach (var item in allNumbers)
        {
            for (int i = 0; i < number.Length; i++)
            {
                if (item == number[i])
                    cows++;
            }
        }

        return cows;
    }

    public int OnCheckBulls()
    {
        int bulls = 0;
        string result = yourChoose.text;
        int[] number = new int[result.Length];
        for (int i = 0; i < number.Length; i++)
        {
            number[i] = int.Parse(result[i].ToString());
        }
        for (int i = 0; i < allNumbers.Count; i++)
        {
            if (allNumbers[i] == number[i])
            {
                bulls++;
            }
        }

        return bulls;
    }

    public void OnCheck()
    {
        int bulls = OnCheckBulls();
        if (bulls == limitNumbers)
            finishPanel.SetActive(true);

        Debug.Log("Bulls" + bulls);
        int cows = OnCheckCows() - bulls;
        Debug.Log("Cows" + cows);
        yourChoose.text = "";

        sourceText.text = "Cows:" + cows + ", Bulls:" + bulls;

        OnClickButton();
        Init();

        startState = true;
    }


    public void OnLoadScene()
    {
        SceneManager.LoadScene(0);
    }
}
