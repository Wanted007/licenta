﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ShowStatistics : MonoBehaviour
{

    public Text mPercentageDay;
    public Text mDescriptionDay;

    public Text mPercentageWeek;
    public Text mDescriptionWeek;

    public Text mPercentageMonth;
    public Text mDescriptionMonth;

    void OnEnable()
    {
        OnCalculate();
    }
    void OnCalculate()
    {
        //Debug.Log("Ziua:" + PlayerPrefs.GetInt(Performance.DayKey));
        //Debug.Log(PlayerPrefs.GetFloat(Performance.BonusDayKey));
        //Debug.Log("ZiuaAnterioară:" + PlayerPrefs.GetInt(Performance.LastDayPlayedKey));
        //Debug.Log(PlayerPrefs.GetFloat(Performance.BonusLastDayPlayedKey));

        mDescriptionDay.text = "Today: " + PlayerPrefs.GetFloat(Performance.BonusDayKey) + " points\nLast day played: " + PlayerPrefs.GetFloat(Performance.BonusLastDayPlayedKey) + " points";
        float tmpPercentageDay = (PlayerPrefs.GetFloat(Performance.BonusDayKey) / PlayerPrefs.GetFloat(Performance.BonusLastDayPlayedKey)) * 100 - 100;
        tmpPercentageDay = float.Parse(Math.Round(tmpPercentageDay, 2).ToString());
        if (PlayerPrefs.GetFloat(Performance.BonusLastDayPlayedKey) > 0)
        {
            mPercentageDay.text = tmpPercentageDay.ToString() + "%";
        }
        else
        {
            mPercentageDay.text = "---";
        }


        //Debug.Log("Saptamana:" + PlayerPrefs.GetInt(Performance.WeekKey));
        //Debug.Log(PlayerPrefs.GetFloat(Performance.BonusWeekKey));
        mDescriptionWeek.text = "This week: " + PlayerPrefs.GetFloat(Performance.BonusWeekKey) + " points\nLast week played: " + PlayerPrefs.GetFloat(Performance.BonusLastWeekPlayedKey) + " points";
        float tmpPercentageWeek = (PlayerPrefs.GetFloat(Performance.BonusWeekKey) / PlayerPrefs.GetFloat(Performance.BonusLastWeekPlayedKey)) * 100 - 100;
        tmpPercentageWeek = float.Parse(Math.Round(tmpPercentageWeek, 2).ToString());
        if (PlayerPrefs.GetFloat(Performance.BonusLastWeekPlayedKey) > 0)
        {
            mPercentageWeek.text = tmpPercentageWeek.ToString() + "%";
        }
        else
        {
            mPercentageWeek.text = "---";
        }

        //Debug.Log("Luna:" + PlayerPrefs.GetInt(Performance.MonthKey));
        //Debug.Log(PlayerPrefs.GetFloat(Performance.BonusMonthKey));
        mDescriptionMonth.text = "This month: " + PlayerPrefs.GetFloat(Performance.BonusMonthKey) + " points\nLast month played: " + PlayerPrefs.GetFloat(Performance.BonusLastMonthPlayedKey) + " points";
        float tmpPercentageMonth = (PlayerPrefs.GetFloat(Performance.BonusMonthKey) / PlayerPrefs.GetFloat(Performance.BonusLastMonthPlayedKey)) * 100 - 100;
        tmpPercentageMonth = float.Parse(Math.Round(tmpPercentageMonth, 2).ToString());
        if (PlayerPrefs.GetFloat(Performance.BonusLastMonthPlayedKey) > 0)
        {
            mPercentageMonth.text = tmpPercentageMonth.ToString() + "%";
        }
        else
        {
            mPercentageMonth.text = "---";
        }
    }

    [ContextMenu("DeleteMonth")]
   void DeleteD()
    {
        PlayerPrefs.DeleteKey(Performance.BonusLastMonthPlayedKey);
    }
    
}
