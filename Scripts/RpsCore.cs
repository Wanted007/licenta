﻿using System;
using System.Collections;
using Photon;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

using ExitGames.Client.Photon;

// the Photon server assigns a ActorNumber (player.ID) to each player, beginning at 1
// for this game, we don't mind the actual number
// this game uses player 0 and 1, so clients need to figure out their number somehow
public class RpsCore : PunBehaviour, IPunTurnManagerCallbacks
{

    [SerializeField]
    private RectTransform ConnectUiView;

    [SerializeField]
    private RectTransform GameUiView;

    [SerializeField]
    private Button btnInteraction;

    [SerializeField]
    private RectTransform TimerFillImage;

    [SerializeField]
    private Text TurnText;

    [SerializeField]
    private Text TimeText;

    [SerializeField]
    private Text RemotePlayerText;

    [SerializeField]
    private Text LocalPlayerText;


    public GameObject feedbackPanel;

    [SerializeField]
    private RectTransform DisconnectedPanel;

    [SerializeField]
    private InputField yourCode;

    [SerializeField]
    private Text debugText;


    //New fields
    //public InputField userChoose;
    public KeyboardFunctionality keyboardInstance=null;
    PhotonPlayer remote;
    PhotonPlayer player;
    string codePlayer1 = "";
    string codePlayer2 = "";
    public Text feedbackPlayers;
    public Text winText;
    public GameObject winPenal;
    //public Text debugText1;
    //public Text debugText2;

    private PunTurnManager turnManager;


    // keep track of when we show the results to handle game logic.
    private bool IsShowingResults;




    public void Start()
    {
        //keyboardInstance = KeyboardFunctionality.GetInstance();
        this.turnManager = this.gameObject.AddComponent<PunTurnManager>();
        this.turnManager.TurnManagerListener = this;
        this.turnManager.TurnDuration = 30f;

        RefreshUIViews();
    }
    public void OnCalculateScore()
    {

        string result = keyboardInstance.OnCheckAnswer();
        int bulls = OnCheckBulls(result, codePlayer2);
        int cows = OnCheckCows(result, codePlayer2) - bulls;
        UpdateInformation(cows, bulls);


    }


    public void UpdateInformation(int _cows, int _bulls)
    {
        ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable();
        hashTable.Add("PlayerName", PhotonNetwork.player.NickName);
        hashTable.Add("Cows", _cows.ToString());
        hashTable.Add("Bulls", _bulls.ToString());
        PhotonNetwork.player.SetCustomProperties(hashTable);

    }
    public void ShowGUIResult()
    {
        remote = PhotonNetwork.player.GetNext();
        player = PhotonNetwork.player;
        ExitGames.Client.Photon.Hashtable hashTablePlayer1 = new ExitGames.Client.Photon.Hashtable();
        ExitGames.Client.Photon.Hashtable hashTablePlayer2 = new ExitGames.Client.Photon.Hashtable();

        hashTablePlayer1 = player.CustomProperties;
        hashTablePlayer2 = remote.CustomProperties;

        //foreach (string item in hashTablePlayer1.Values)
        //{

        //    debugText1.text += item;
        //}
        //foreach (string item in hashTablePlayer2.Values)
        //{

        //    debugText2.text += item;
        //}

        try
        {
            string numePlayer1 = "";
            int cowsPlayer1 = 0;
            int bullsPlayer1 = 0;
            string numePlayer2 = "";
            int cowsPlayer2 = 0;
            int bullsPlayer2 = 0;

            if (hashTablePlayer1.ContainsKey("PlayerName"))
            {
                numePlayer1 = hashTablePlayer1["PlayerName"].ToString();
            }
            if (hashTablePlayer1.ContainsKey("Cows"))
            {
                cowsPlayer1 = int.Parse(hashTablePlayer1["Cows"].ToString());
            }
            if (hashTablePlayer1.ContainsKey("Bulls"))
            {
                bullsPlayer1 = int.Parse(hashTablePlayer1["Bulls"].ToString());
            }
            if (hashTablePlayer2.ContainsKey("PlayerName"))
            {
                numePlayer2 = hashTablePlayer2["PlayerName"].ToString();
            }
            if (hashTablePlayer2.ContainsKey("Cows"))
            {
                cowsPlayer2 = int.Parse(hashTablePlayer2["Cows"].ToString());
            }
            if (hashTablePlayer2.ContainsKey("Bulls"))
            {
                bullsPlayer2 = int.Parse(hashTablePlayer2["Bulls"].ToString());
            }
            if(bullsPlayer1==GlobalParameters.lengthList)
            {
                winText.text = numePlayer1 + " win!";
                winPenal.SetActive(true);
            }
            else if(bullsPlayer2 == GlobalParameters.lengthList)
            {
                winText.text = numePlayer2 + " win!";
                winPenal.SetActive(true);
            }
            feedbackPlayers.text = numePlayer1 + " have:" + " " + cowsPlayer1 + " cows and " + bullsPlayer1 + " bulls" + "\n " + numePlayer2 + " have:" + " " + cowsPlayer2 + " cows and " + bullsPlayer2 + " bulls";
        }

        catch (Exception exception)
        {
            Debug.Log(exception.Message);
        }
    }


    [ContextMenu("ShowInfo")]
    public void ShowInfo()
    {
        try
        {
            remote = PhotonNetwork.player.GetNext();
            player = PhotonNetwork.player;
            ExitGames.Client.Photon.Hashtable hashTablePlayer1 = new ExitGames.Client.Photon.Hashtable();
            ExitGames.Client.Photon.Hashtable hashTablePlayer2 = new ExitGames.Client.Photon.Hashtable();

            hashTablePlayer1 = player.CustomProperties;
            hashTablePlayer2 = remote.CustomProperties;

            foreach (string item in hashTablePlayer1.Values)
            {
                Debug.Log(item);
                codePlayer1 = item;
            }
            foreach (string item in hashTablePlayer2.Values)
            {
                Debug.Log(item);
                codePlayer2 = item;
            }

        }
        catch (Exception exception)
        {
            Debug.Log(exception.Message);
        }

    }
    public int OnCheckCows(string _userChoose, string _correctCode)
    {
        int cows = 0;
        try
        {
            for (int i = 0; i < _userChoose.Length; i++)
            {
                for (int j = 0; j < _correctCode.Length; j++)
                {
                    if (_userChoose[i] == _correctCode[j])
                    {
                        cows++;
                    }
                }
            }
        }
        catch (Exception exception)
        {
            Debug.Log(exception.Message);
        }

        return cows;
    }
    public int OnCheckBulls(string _userChoose, string _correctCode)
    {
        int bulls = 0;
        try
        {
            for (int i = 0; i < _userChoose.Length; i++)
            {
                if (_userChoose[i] == _correctCode[i])
                {
                    bulls++;
                }
            }
        }
        catch (Exception exception)
        {
            Debug.Log(exception.Message);
        }

        return bulls;
    }
    public void Update()
    {
        // Check if we are out of context, which means we likely got back to the demo hub.
        if (this.DisconnectedPanel == null)
        {
            Destroy(this.gameObject);
        }


        if (!PhotonNetwork.inRoom)
        {
            return;
        }

        // disable the "reconnect panel" if PUN is connected or connecting
        if (PhotonNetwork.connected && this.DisconnectedPanel.gameObject.GetActive())
        {
            this.DisconnectedPanel.gameObject.SetActive(false);
        }
        if (!PhotonNetwork.connected && !PhotonNetwork.connecting && !this.DisconnectedPanel.gameObject.GetActive())
        {
            this.DisconnectedPanel.gameObject.SetActive(true);
        }


        if (PhotonNetwork.room.PlayerCount > 1)
        {
            if (this.turnManager.IsOver)
            {
                return;
            }

            if (this.TurnText != null)
            {
                this.TurnText.text = this.turnManager.Turn.ToString();
            }

            if (this.turnManager.Turn > 0 && this.TimeText != null && !IsShowingResults)
            {

                this.TimeText.text = this.turnManager.RemainingSecondsInTurn.ToString("F1") + " SECONDS";

                TimerFillImage.anchorMax = new Vector2(1f - this.turnManager.RemainingSecondsInTurn / this.turnManager.TurnDuration, 1f);
            }


        }

        this.UpdatePlayerTexts();

    }


    public void OnTurnBegins(int turn)
    {

        feedbackPanel.SetActive(false);
        IsShowingResults = false;
        btnInteraction.interactable = true;
    }


    public void OnTurnCompleted(int obj)
    {
        this.OnEndTurn();
    }


    // when a player moved (but did not finish the turn)
    public void OnPlayerMove(PhotonPlayer photonPlayer, int turn, object move)
    {
        //Unimplemented yed
    }


    // when a player made the last/final move in a turn
    public void OnPlayerFinished(PhotonPlayer photonPlayer, int turn, object move)
    {
        //Unimplemented yed
    }



    public void OnTurnTimeEnds(int obj)
    {

        if (!IsShowingResults)
        {
            OnTurnCompleted(-1);
        }
    }




    public void StartTurn()
    {
        this.turnManager.BeginTurn();
    }

    public void MakeTurn()
    {
        this.turnManager.SendMove("ThatMove", true);
    }

    public void OnEndTurn()
    {

        this.StartCoroutine("ShowResultsBeginNextTurnCoroutine");
    }

    public IEnumerator ShowResultsBeginNextTurnCoroutine()
    {
        btnInteraction.interactable = false;
        IsShowingResults = true;
        OnCalculateScore();
        yield return new WaitForSeconds(1.5f);

        ShowGUIResult();
        yield return new WaitForSeconds(1.5f);
        this.feedbackPanel.SetActive(true);

        yield break;
    }
    public void OnNewTurn()
    {
        StopCoroutine("ShowResultsBeginNextTurnCoroutine");
        this.StartTurn();
    }

    private void UpdatePlayerTexts()
    {
        PhotonPlayer remote = PhotonNetwork.player.GetNext();
        PhotonPlayer local = PhotonNetwork.player;

        if (remote != null)
        {
            this.RemotePlayerText.text = remote.NickName;
        }
        else
        {
            TimerFillImage.anchorMax = new Vector2(0f, 1f);
            this.TimeText.text = "";
            this.RemotePlayerText.text = "waiting for another player";
        }

        if (local != null)
        {
            this.LocalPlayerText.text = "YOU";
        }
    }

    public void OnDoTurn()
    {
        this.MakeTurn();
    }

    //to timeout in background!
    public void OnClickConnect()
    {
        PhotonNetwork.ConnectUsingSettings(null);
        PhotonHandler.StopFallbackSendAckThread();  
    }

    //to timeout in background!
    public void OnClickReConnectAndRejoin()
    {
        PhotonNetwork.ReconnectAndRejoin();
        PhotonHandler.StopFallbackSendAckThread();  
    }

    public void OnDisconnect()
    {
        //PhotonNetwork.Disconnect();
    }
    void RefreshUIViews()
    {

        TimerFillImage.anchorMax = new Vector2(0f, 1f);

        ConnectUiView.gameObject.SetActive(!PhotonNetwork.inRoom);
        GameUiView.gameObject.SetActive(PhotonNetwork.inRoom);

        btnInteraction.interactable = PhotonNetwork.room != null ? PhotonNetwork.room.PlayerCount > 1 : false;
    }


    public override void OnLeftRoom()
    {
        RefreshUIViews();
    }

    public override void OnJoinedRoom()
    {

        RefreshUIViews();

        if (PhotonNetwork.room.PlayerCount == 2)
        {
            if (this.turnManager.Turn == 0)
            {
                this.StartTurn();
                this.ShowInfo();
            }
        }

    }

    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        if (PhotonNetwork.room.PlayerCount == 2)
        {
            if (this.turnManager.Turn == 0)
            {
                this.StartTurn();
                this.ShowInfo();
            }
        }
    }

    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        Debug.Log("Other player disconnected! " + otherPlayer.ToStringFull());
    }

    public override void OnConnectionFail(DisconnectCause cause)
    {
        this.DisconnectedPanel.gameObject.SetActive(true);
    }

}
