﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneFunctionality : MonoBehaviour {

    public bool isStartScene = false;
    public GameObject currentStep;
    public GameObject prevStep;
    void Awake()
    {
        if (isStartScene && (PlayerPrefs.GetString("SceneLoaded")=="Yes"))
        {
            currentStep.SetActive(true);
            prevStep.SetActive(false);
        }
    }
    public void OnCloseApplication()
    {
        PlayerPrefs.SetString("SceneLoaded", "No");
        Application.Quit();
        
    }
    public void OnChangeScene(string _nameScene)
    {
        SceneManager.LoadScene(_nameScene);
        PlayerPrefs.SetString("SceneLoaded","Yes");
    }

}
