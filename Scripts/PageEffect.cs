﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PageEffect : MonoBehaviour
{

    public GameObject page;
    public CanvasGroup canvasGroup;
    public float valueAnimation= 0.05f;
    public float speedAnimation = 0.01f;
	public bool resetValues=false;

    [ContextMenu("DisablePage")]
    public void OnDisablePage()
    {
        StartCoroutine(StartAnimation(false));
        
    }

    [ContextMenu("EnablePage")]
    public void OnEnablePage()
    {
        StartCoroutine(StartAnimation(true));
    }

    void OnEnable()
    {
		gameObject.transform.SetAsLastSibling ();
		StartCoroutine(StartAnimation(true));

    }

	void OnDisable()
	{
		if (resetValues)
		{
			page.transform.localScale = Vector3.zero;
			canvasGroup.alpha=0f;
		
		}

	}
    IEnumerator StartAnimation(bool _state)
    {

        if (_state)
        {
            while (page.transform.localScale != Vector3.one)
            {
                page.transform.localScale += new Vector3(valueAnimation, valueAnimation, valueAnimation);
                canvasGroup.alpha += valueAnimation;
                yield return new WaitForSecondsRealtime(speedAnimation);
            }
            
        }
        else
        {
            while (page.transform.localScale != Vector3.zero)
            {
                page.transform.localScale -= new Vector3(valueAnimation, valueAnimation, valueAnimation);
                canvasGroup.alpha -= valueAnimation;
                yield return new WaitForSecondsRealtime(speedAnimation);
            }
            this.gameObject.SetActive(false);
        }


        yield return null;
    }
}
