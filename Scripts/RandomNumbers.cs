﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RandomNumbers : MonoBehaviour
{
    [Tooltip("Keyword used in PlayerPrefs. Must be the same with the one used in GamificationManager.cs Script")]
    public string GamesPlayedKeyPlayerPrefs = "GamesPlayed";

    public bool debugMode = true;

    public GameObject winPanel;
    public GameObject scrollFeedback;
    public bool hintsMode = false;
    public int nrHints = 5;
    //Variables used for First Method
    List<int> randomListOnlyNumbers = new List<int>();
    string generatedCode = "";
    InputLogic inputLogic = null;
    KeyboardFunctionality keyboardInstance;
    AudioManager audioManager = null;
    public ShowAllTries instance;
    public int limitBulls;
    public int limitCows;

    public GameObject numberKeyboard;
    public GameObject letterKeyboard;
    public GameObject alphaKeyboard;

    int tmpGamesLettersTotal = 0;
    int tmpGamesLettersLost = 0;

    int tmpGamesNumbersTotal = 0;
    int tmpGamesNumbersLost = 0;

    int tmpGamesAlphaTotal = 0;
    int tmpGamesAlphaLost = 0;

    void Start()
    {
        inputLogic = InputLogic.GetInstance();
        keyboardInstance = KeyboardFunctionality.GetInstance();
        audioManager = AudioManager.GetInstance();
    }
    void OnEnable()
    {
        //OnGenerateCodes();
        

    }

    [ContextMenu("DeleteKeys")]
    public void Del()
    {
        PlayerPrefs.DeleteKey(Performance.TotalGamesPlayed);
        PlayerPrefs.DeleteKey(Performance.GamesNumbersTotal);
        PlayerPrefs.DeleteKey(Performance.GamesNumbersLost);
        PlayerPrefs.DeleteKey(Performance.GamesLettersTotal);
        PlayerPrefs.DeleteKey(Performance.GamesLettersLost);
        PlayerPrefs.DeleteKey(Performance.GamesAlphaTotal);
        PlayerPrefs.DeleteKey(Performance.GamesAlphaLost);
    }
    public void OnGenerateCodes()
    {
        int tmpGamesPlayed = 0;
        if (PlayerPrefs.HasKey(Performance.TotalGamesPlayed))
        {
            tmpGamesPlayed = PlayerPrefs.GetInt(Performance.TotalGamesPlayed);
        }
        PlayerPrefs.SetInt(Performance.TotalGamesPlayed, tmpGamesPlayed + 1);

        if (hintsMode)
        {
            Debug.Log("ApelatHint_v2");
            limitBulls = GlobalParameters.lengthList / 2;
            limitCows = GlobalParameters.lengthList / 2;
            OnGeneratHints();
            return;
        }


        if (GlobalParameters.gameType == GameType.Numeric)
        {
            if (PlayerPrefs.HasKey(Performance.GamesNumbersTotal))
            {
                tmpGamesNumbersTotal = PlayerPrefs.GetInt(Performance.GamesNumbersTotal);
            }
            PlayerPrefs.SetInt(Performance.GamesNumbersTotal, tmpGamesNumbersTotal + 1);

            if (PlayerPrefs.HasKey(Performance.GamesNumbersLost))
            {
                tmpGamesNumbersLost = PlayerPrefs.GetInt(Performance.GamesNumbersLost);
            }
            PlayerPrefs.SetInt(Performance.GamesNumbersLost, tmpGamesNumbersLost + 1);

            numberKeyboard.SetActive(true);
            letterKeyboard.SetActive(false);
            alphaKeyboard.SetActive(false);

            GetRandomAlphaNumeric(true);
        }
        else if (GlobalParameters.gameType == GameType.Letters)
        {
            if (PlayerPrefs.HasKey(Performance.GamesLettersTotal))
            {
                tmpGamesLettersTotal = PlayerPrefs.GetInt(Performance.GamesLettersTotal);
            }
            PlayerPrefs.SetInt(Performance.GamesLettersTotal, tmpGamesLettersTotal + 1);

            if (PlayerPrefs.HasKey(Performance.GamesLettersLost))
            {
                tmpGamesLettersLost = PlayerPrefs.GetInt(Performance.GamesLettersLost);
            }
            PlayerPrefs.SetInt(Performance.GamesLettersLost, tmpGamesLettersLost + 1);

            numberKeyboard.SetActive(false);
            letterKeyboard.SetActive(true);
            alphaKeyboard.SetActive(false);

            GetRandomAlphaNumeric(false);

        }
        else if (GlobalParameters.gameType == GameType.AlphaNumeric)
        {
            if (PlayerPrefs.HasKey(Performance.GamesAlphaTotal))
            {
                tmpGamesAlphaTotal = PlayerPrefs.GetInt(Performance.GamesAlphaTotal);
            }
            PlayerPrefs.SetInt(Performance.GamesAlphaTotal, tmpGamesAlphaTotal + 1);

            if (PlayerPrefs.HasKey(Performance.GamesAlphaLost))
            {
                tmpGamesAlphaLost = PlayerPrefs.GetInt(Performance.GamesAlphaLost);
            }
            PlayerPrefs.SetInt(Performance.GamesAlphaLost, tmpGamesAlphaLost + 1);
            numberKeyboard.SetActive(false);
            letterKeyboard.SetActive(false);
            alphaKeyboard.SetActive(true);

            GetRandomOnlyAlphaNumeric();
        }
    }
    [ContextMenu("GenerateNumbersMethod")]
    public void GenerateOnlyNumbers()
    {
        int number = 0;
        while (randomListOnlyNumbers.Count < GlobalParameters.lengthList)
        {
            number = Random.Range(0, 10);
            if (!randomListOnlyNumbers.Contains(number))
                randomListOnlyNumbers.Add(number);
        }

        if (debugMode)
        {
            ShowNumber<int>(randomListOnlyNumbers.ToArray(), GlobalParameters.lengthList);
        }

    }

    [ContextMenu("GenerateNumbersMethod2")]
    public void GetRandomAlphaNumeric(bool _onlyNumbers, bool _hint = false)
    {
        if (_onlyNumbers)
        {
            int[] array = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Shuffle(array);
            if (_hint)
            {
                ShowNumber2<int>(array, GlobalParameters.lengthList);
                return;
            }
            ShowNumber<int>(array, GlobalParameters.lengthList);
        }
        else
        {
            string[] chars = {
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W",
                "X",
                "Y",
                "Z"
            };
            Shuffle<string>(chars);
            if (_hint)
            {
                ShowNumber2<string>(chars, GlobalParameters.lengthList);
                return;
            }
            ShowNumber<string>(chars, GlobalParameters.lengthList);
        }

    }
    public void GetRandomOnlyAlphaNumeric(bool _hint = false)
    {

        int[] array1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        Shuffle(array1);
        string[] chars = {
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W",
                "X",
                "Y",
                "Z"
        };
        Shuffle<string>(chars);
        int tmpLetters = 0;
        int tmpNumbers = 0;
        string result = "";
        if ((GlobalParameters.lengthList % 2) == 0)
        {
            tmpLetters = tmpNumbers = GlobalParameters.lengthList / 2;
            for (int i = 0; i < tmpLetters; i++)
            {
                result += chars[i].ToString();
                result += array1[i].ToString();
            }
        }
        else
        {
            tmpNumbers = GlobalParameters.lengthList / 2;
            tmpLetters = tmpNumbers + 1;
            for (int i = 0; i < tmpLetters - 1; i++)
            {
                result += chars[i].ToString();
                result += array1[i].ToString();
            }
            result += chars[tmpLetters - 1].ToString();
        }
        generatedCode = result;
        GlobalParameters.currentCode = result;
        Debug.Log("Resultat:" + result);
    }



    /*
     * Fisher-Yates shuffle.Shuffling an array randomizes its element order.
    */
    void Shuffle<T>(T[] array)
    {
        System.Random random = new System.Random();
        int n = array.Length;
        for (int i = 0; i < n; i++)
        {
            int r = i + random.Next(n - i);
            T t = array[r];
            array[r] = array[i];
            array[i] = t;
        }
    }


    void ShowNumber<T>(T[] array, int _length = 0)
    {
        _length = _length == 0 ? array.Length : _length;
        string result = "";
        for (int i = 0; i < _length; i++)
        {
            result += array[i].ToString();
        }
        generatedCode = result;
        GlobalParameters.currentCode = generatedCode;
        Debug.Log("Resultat:" + result);
    }

    void ShowNumber2<T>(T[] array, int _length = 0)
    {
        _length = _length == 0 ? array.Length : _length;
        string result = "";
        for (int i = 0; i < _length; i++)
        {
            result += array[i].ToString();
        }

        GlobalParameters.hintCode = result;
        Debug.Log("Resultat:" + result);
    }

    [ContextMenu("ShowCows")]
    public int OnCheckCows()
    {
        int cows = 0;
        string result = keyboardInstance.OnCheckAnswer();

        for (int i = 0; i < result.Length; i++)
        {
            for (int j = 0; j < generatedCode.Length; j++)
            {
                if (result[i] == generatedCode[j])
                {
                    cows++;
                }
            }
        }

        return cows;
    }
    public int OnCheckCows(string _result, string _generatedCode)
    {
        int cows = 0;


        for (int i = 0; i < _result.Length; i++)
        {
            for (int j = 0; j < _generatedCode.Length; j++)
            {
                if (_result[i] == _generatedCode[j])
                {
                    cows++;
                }
            }
        }

        return cows;
    }

    [ContextMenu("ShowResults")]
    public void ShowResult()
    {
        //inputLogic.OnCheckAnswer();

        string result = keyboardInstance.OnCheckAnswer();
        int bulls = OnCheckBulls();
        int cows = OnCheckCows() - bulls;

        if (bulls == GlobalParameters.lengthList)
        {
            if (GlobalParameters.gameType == GameType.Numeric)
            {
                tmpGamesNumbersLost = PlayerPrefs.GetInt(Performance.GamesNumbersLost);
                PlayerPrefs.SetInt(Performance.GamesNumbersLost, tmpGamesNumbersLost - 1);
            }
            else if (GlobalParameters.gameType == GameType.Letters)
            {
                tmpGamesLettersLost = PlayerPrefs.GetInt(Performance.GamesLettersLost);
                PlayerPrefs.SetInt(Performance.GamesLettersLost, tmpGamesLettersLost - 1);
            }
            else if (GlobalParameters.gameType == GameType.AlphaNumeric)
            {
                tmpGamesAlphaLost = PlayerPrefs.GetInt(Performance.GamesAlphaLost);
                PlayerPrefs.SetInt(Performance.GamesAlphaLost, tmpGamesAlphaLost - 1);
            }



            if (PlayerPrefs.HasKey(GamesPlayedKeyPlayerPrefs))
            {
                int gamesPlayed = PlayerPrefs.GetInt(GamesPlayedKeyPlayerPrefs);
                gamesPlayed += 1;
                PlayerPrefs.SetInt(GamesPlayedKeyPlayerPrefs, gamesPlayed);
            }
            else
            {
                PlayerPrefs.SetInt(GamesPlayedKeyPlayerPrefs, 1);
            }
            winPanel.SetActive(true);
            audioManager.OnWin();
            return;
        }
        audioManager.OnLose();
        GlobalParameters.currentResult = result + "," + bulls + "," + cows;
        scrollFeedback.GetComponent<ShowAllTries>().OnInstantiateLastTry();
    }
    public int OnCheckBulls()
    {
        int bulls = 0;
        string result = keyboardInstance.OnCheckAnswer();
        Debug.Log(result + " si " + generatedCode);
        for (int i = 0; i < result.Length; i++)
        {
            if (result[i] == generatedCode[i])
            {
                bulls++;
            }
        }

        return bulls;
    }
    public int OnCheckBulls(string _result, string _generatedCode)
    {
        int bulls = 0;
        for (int i = 0; i < _result.Length; i++)
        {
            if (_result[i] == _generatedCode[i])
            {
                bulls++;
            }
        }

        return bulls;
    }

    public void OnGeneratHints()
    {
        int bulls;
        int cows;
        if (GlobalParameters.gameType == GameType.Numeric)
        {
            GetRandomAlphaNumeric(true);
            int i = 0;
            while (i < nrHints)
            {
                GetRandomAlphaNumeric(true, true);
                bulls = OnCheckBulls(GlobalParameters.hintCode, GlobalParameters.currentCode);
                cows = OnCheckCows(GlobalParameters.hintCode, GlobalParameters.currentCode) - bulls;
                if (GlobalParameters.hintsCode.Contains(GlobalParameters.hintCode) || bulls > limitBulls || bulls == 0 || cows == 0 || cows > limitCows)
                {
                    GetRandomAlphaNumeric(true, true);
                }
                else
                {
                    GlobalParameters.hintsCode.Add(GlobalParameters.hintCode);
                    i++;
                    bulls = OnCheckBulls(GlobalParameters.hintCode, GlobalParameters.currentCode);
                    cows = OnCheckCows(GlobalParameters.hintCode, GlobalParameters.currentCode) - bulls;
                    GlobalParameters.currentResult = GlobalParameters.hintCode + "," + bulls + "," + cows;
                    instance.OnInstantiateLastTry();
                }
            }
            //for (int i = 0; i < nrHints; i++)
            //{
            //    GetRandomAlphaNumeric(true,true);
            //    bulls = OnCheckBulls(GlobalParameters.hintCode,GlobalParameters.currentCode);
            //    cows = OnCheckCows(GlobalParameters.hintCode, GlobalParameters.currentCode) - bulls;
            //    GlobalParameters.currentResult = GlobalParameters.hintCode + "," + bulls + "," + cows;
            //    instance.OnInstantiateLastTry();
            //}
        }
        else if (GlobalParameters.gameType == GameType.Letters)
        {
            GetRandomAlphaNumeric(false);
            int i = 0;
            while (i < nrHints)
            {
                GetRandomAlphaNumeric(false, true);
                if (GlobalParameters.hintsCode.Contains(GlobalParameters.hintCode))
                {
                    GetRandomAlphaNumeric(false, true);
                }
                else
                {
                    GlobalParameters.hintsCode.Add(GlobalParameters.hintCode);
                    i++;
                    bulls = OnCheckBulls(GlobalParameters.hintCode, GlobalParameters.currentCode);
                    cows = OnCheckCows(GlobalParameters.hintCode, GlobalParameters.currentCode) - bulls;
                    GlobalParameters.currentResult = GlobalParameters.hintCode + "," + bulls + "," + cows;
                    instance.OnInstantiateLastTry();
                }
            }
            //for (int i = 0; i < nrHints; i++)
            //{
            //    GetRandomAlphaNumeric(false, true);
            //    bulls = OnCheckBulls(GlobalParameters.hintCode, GlobalParameters.currentCode);
            //    cows = OnCheckCows(GlobalParameters.hintCode, GlobalParameters.currentCode) - bulls;
            //    GlobalParameters.currentResult = GlobalParameters.hintCode + "," + bulls + "," + cows;
            //    instance.OnInstantiateLastTry();
            //}

        }

    }

}
