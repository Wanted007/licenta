﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ShowPercentagePerformance : MonoBehaviour
{

    public Text mGamesPlayed;

    public Text mPercentageNumbersGamesText;
    public Image mPercentageNumbersGamesImage;

    public Text mPercentageLettersGamesText;
    public Image mPercentageLettersGamesImage;

    public Text mPercentageAlphaGamesText;
    public Image mPercentageAlphaGamesImage;

    public Text mNameNumbersGamesText;
    public Text mNameLettersGamesText;
    public Text mNameAlphaGamesText;

    public Text mPercentageAllGames;
    public Text mDescriptionAllGames;
    public Image mPercentageAllGamesImage;

    public Text mPercentageBestAllGames;
    public Text mDescriptionBestAllGames;
    public Image mPercentageBestAllGamesImage;

    public Text mTotalPoints;
    void OnEnable()
    {
        if (PlayerPrefs.HasKey(Performance.TotalGamesPlayed))
        {
            mGamesPlayed.text = "Games Played:" + PlayerPrefs.GetInt(Performance.TotalGamesPlayed).ToString();
        }

        if (PlayerPrefs.HasKey(Performance.GamesLettersTotal))
        {
            int tmpAllGames = PlayerPrefs.GetInt(Performance.GamesLettersTotal);
            int tmpLostGames = PlayerPrefs.GetInt(Performance.GamesLettersLost);
            mNameLettersGamesText.text = "Letters Games Played: " + tmpAllGames;
            if (tmpAllGames > 0)
            {
                int tmpWonGames = tmpAllGames - tmpLostGames;
                float tmpPercentage = ((float)tmpWonGames / (float)tmpAllGames) * 100;
                tmpPercentage = float.Parse(Math.Round(tmpPercentage, 2).ToString());

                mPercentageLettersGamesText.text = tmpPercentage.ToString() + "%";
                tmpPercentage = (float)tmpWonGames / (float)tmpAllGames;
                mPercentageLettersGamesImage.fillAmount = tmpPercentage;
            }
            else
            {
                mPercentageLettersGamesText.text = "---";
                mPercentageLettersGamesImage.fillAmount = 1f;
            }
        }
        if (PlayerPrefs.HasKey(Performance.GamesNumbersTotal))
        {
            int tmpAllGames = PlayerPrefs.GetInt(Performance.GamesNumbersTotal);
            int tmpLostGames = PlayerPrefs.GetInt(Performance.GamesNumbersLost);
            mNameNumbersGamesText.text = "Numbers Games Played: " + tmpAllGames;
            if (tmpAllGames > 0)
            {
                int tmpWonGames = tmpAllGames - tmpLostGames;
                float tmpPercentage = ((float)tmpWonGames / (float)tmpAllGames) * 100;
                Debug.Log("Percentage:" + tmpPercentage);
                tmpPercentage = float.Parse(Math.Round(tmpPercentage, 2).ToString());

                mPercentageNumbersGamesText.text = tmpPercentage.ToString() + "%";
                tmpPercentage = (float)tmpWonGames / (float)tmpAllGames;
                mPercentageNumbersGamesImage.fillAmount = tmpPercentage;
            }
            else
            {
                mPercentageNumbersGamesText.text = "---";
                mPercentageNumbersGamesImage.fillAmount = 1f;
            }
        }
        if (PlayerPrefs.HasKey(Performance.GamesAlphaTotal))
        {
            int tmpAllGames = PlayerPrefs.GetInt(Performance.GamesAlphaTotal);
            int tmpLostGames = PlayerPrefs.GetInt(Performance.GamesAlphaLost);
            mNameAlphaGamesText.text = "AlphaNumeric Games Played: " + tmpAllGames;
            if (tmpAllGames > 0)
            {
                int tmpWonGames = tmpAllGames - tmpLostGames;
                float tmpPercentage = ((float)tmpWonGames / (float)tmpAllGames) * 100;
                tmpPercentage = float.Parse(Math.Round(tmpPercentage, 2).ToString());

                mPercentageAlphaGamesText.text = tmpPercentage.ToString() + "%";
                tmpPercentage = (float)tmpWonGames / (float)tmpAllGames;
                mPercentageAlphaGamesImage.fillAmount = tmpPercentage;
            }
            else
            {
                mPercentageAlphaGamesText.text = "---";
                mPercentageAlphaGamesImage.fillAmount = 1f;
            }
        }

        if (PlayerPrefs.HasKey(Performance.TotalBonus))
        {
            mTotalPoints.text = PlayerPrefs.GetFloat(Performance.TotalBonus).ToString() + " points";
        }
        else
        {
            mTotalPoints.text = "0 points";
        }

        CalculateGlobalPercentage();
    }

    void CalculateGlobalPercentage()
    {
        int allGames = 0;
        int lostLetters = 0;
        int lostNumbers = 0;
        int lostAlpha = 0;

        if (PlayerPrefs.HasKey(Performance.TotalGamesPlayed))
        {
            allGames = PlayerPrefs.GetInt(Performance.TotalGamesPlayed);

            if (PlayerPrefs.HasKey(Performance.GamesLettersLost))
            {
                lostLetters = PlayerPrefs.GetInt(Performance.GamesLettersLost);
            }
            if (PlayerPrefs.HasKey(Performance.GamesNumbersLost))
            {
                lostNumbers = PlayerPrefs.GetInt(Performance.GamesNumbersLost);
            }
            if (PlayerPrefs.HasKey(Performance.GamesAlphaLost))
            {
                lostAlpha = PlayerPrefs.GetInt(Performance.GamesAlphaLost);
            }

            int totalWon = allGames - lostLetters - lostAlpha - lostNumbers;
            mPercentageAllGames.text = (((float)totalWon / (float)allGames)*100).ToString()+"%";
            mDescriptionAllGames.text = "Games played: " + allGames + "\nGames Won: " + totalWon;
            //mPercentageAllGamesImage.fillAmount = (float)totalWon / (float)allGames;

            if (!PlayerPrefs.HasKey(Performance.BestScoreAllGames))
            {
                PlayerPrefs.SetInt(Performance.BestScoreAllGames, allGames);
                PlayerPrefs.SetInt(Performance.BestScoreAllPercentage, totalWon);

                mPercentageBestAllGames.text = mPercentageAllGames.text;
                mDescriptionBestAllGames.text = mDescriptionAllGames.text;
                //mPercentageBestAllGamesImage.fillAmount = mPercentageAllGamesImage.fillAmount;
            }
            else
            {
                float tmpSavedPercentage = ((float)PlayerPrefs.GetInt(Performance.BestScoreAllPercentage) / (float)PlayerPrefs.GetInt(Performance.BestScoreAllGames));
                float tmpActualPercentage = ((float)totalWon / (float)allGames);
                if (tmpActualPercentage >= tmpSavedPercentage)
                {
                    PlayerPrefs.SetInt(Performance.BestScoreAllGames, allGames);
                    PlayerPrefs.SetInt(Performance.BestScoreAllPercentage, totalWon);
                }
                if (tmpSavedPercentage >= tmpActualPercentage)
                {

                    mPercentageBestAllGames.text = (tmpSavedPercentage*100).ToString()+"%";
                    mDescriptionBestAllGames.text = "Games played: " + PlayerPrefs.GetInt(Performance.BestScoreAllGames) + "\nGames Won: " + PlayerPrefs.GetInt(Performance.BestScoreAllPercentage);
                    //mPercentageBestAllGamesImage.fillAmount = tmpSavedPercentage;
                }
                else
                {
                    mPercentageBestAllGames.text = (tmpActualPercentage*100).ToString()+"%";
                    mDescriptionBestAllGames.text = "Games played: " + allGames + "\nGames Won: " + totalWon;
                    //mPercentageBestAllGamesImage.fillAmount = tmpActualPercentage;
                }
            }

        }
        else
        {
            mPercentageAllGames.text = "---";
            mDescriptionAllGames.text = "Games played: 0" + "\nGames Won: 0";
            //mPercentageAllGamesImage.fillAmount = 0f;

            mPercentageBestAllGames.text = "---";
            mDescriptionBestAllGames.text = "Games played: 0" + "\nGames Won: 0";
            //mPercentageBestAllGamesImage.fillAmount = 0f;
        }

    }
}
