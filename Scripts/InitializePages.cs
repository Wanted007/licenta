﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitializePages : MonoBehaviour {

	public GameObject[] allPages;
	public GameObject firstPage;

	[ContextMenu("OnNormalSettings")]
	public void OnNormalSettings()
	{
		foreach (var item in allPages) {
			item.GetComponent<CanvasGroup> ().alpha = 1f;
			item.GetComponent<RectTransform>().localScale = Vector3.one;
			item.SetActive (true);
		}
	}

	[ContextMenu("OnModifiedSettings")]
	public void OnModifiedSettings()
	{
		foreach (var item in allPages) {
			item.GetComponent<CanvasGroup> ().alpha = 0f;
			item.GetComponent<RectTransform>().localScale = Vector3.zero;
			item.SetActive (false);
		}

		firstPage.SetActive (true);
		firstPage.GetComponent<CanvasGroup> ().alpha = 1f;
		firstPage.GetComponent<RectTransform>().localScale = Vector3.one;
	}
}
